# README #

This repository contains the Python Addons by MarsThunder mainly for the Blender 3D modeling application.

The LuxRender Converter is an Addon that installs using the Blender Preferences Addon menu.

If you have landed on Git Source page, you can either download the code directly or use the Downloads link on left menu.

After unzipping the file (how you are reading this), place in an accessible folder.  Inside Blender, open User Preferences and with Addon tab, add the Converter by clicking on 'Add from file...' at bottom of window.  After the Addon installs, activate it by checking the box at right.

The import_obj.py file is a replacement for the included file that comes with Blender 2.78.  If you want to import OBJ files that have material names with spaces, then replace original file in Addons folder io_scene_obj.
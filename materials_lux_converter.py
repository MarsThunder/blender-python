# convert_materials_to_lux.py
# 
# Copyright (C) 5-mar-2012, Based on version by Silvio Falcinelli. Fixes by others. Lux version by Marshall Flynn
#
# special thanks to user blenderartists.org cmomoney
#
# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

bl_info = {
    "name": "Convert Materials to Lux",
    "author": "Marshall Flynn, adapted from original Cycles version by Silvio Falcinelli",
    "version": (1, 7, 0),
    "blender": (2, 78, 0),
    "location": "Properties > Material > Convert Materials to LuxRender",
    "description": "Convert non-nodes Blender Render materials to Lux",
    "warning": "You must have Blender 2.7x and LuxBlend27 Plugin/Addon",
    "support": "COMMUNITY",
    "wiki_url": "http://www.tinkeringconnection.com/wp-content/storage/lux_converter_doc.pdf",
    "category": "Material"}


import bpy
import math
import codecs
from math import log
from math import pow
from math import exp
import os, sys

import time
import datetime
from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )


from luxrender import LuxRenderAddon
print ("Importing modules complete!")
#realpath = os.path.expanduser('c:/temp/blender/sample.jpg')



def AutoNodeOff():
    bpy.context.scene.render.engine = 'BLENDER_RENDER'
    mats = bpy.data.materials
    for nowmat in mats:
        nowmat.use_nodes = False
   

def AutoNode(active=False):
    print ("This is the Lux Material Converter")

    print ("Starting convert here.....")
    bpy.ops.object.mode_set(mode='OBJECT')
    sc = bpy.context.scene
    sc.render.engine = 'BLENDER_RENDER'
    # Clear out BI nodes
    BImats = bpy.data.materials
    for clearmat in BImats:
        clearmat.use_nodes = False
 
    # User choice to omitt glass image mapping:
    map_glass = ''
    if (sc.my_prop == True):
      print ("** Image Map on Glass Disabled")
      map_glass = 'no'
    else:
      print ("** Image Map on Glass Enabled")
      map_glass = 'yes'
    # User choice to convert for LuxCore
    luxcore_run = 'no'  
    if (sc.do_core == True):
      print ("** Prepping for LuxCore API")
      luxcore_run = 'yes'
    else:
      print ("** Using Classic API")
      luxcore_run = 'no'
      
    flatshade_all = 'no'  
    if (sc.do_flat == True):
      print ("** Flat Shading faces for LuxCore")
      flatshade_all = 'yes'
    else:
      print ("** Default shading - perhaps smooth")
      flatshade_all = 'no'
 
    # invoke message
    messageTXT = 'Running...'
  
    # unselect all objects
    for item in bpy.context.selectable_objects:   
           item.select = False   
  
    outpath = bpy.data.scenes[0].render.filepath
    if active:
        # this is redundant in new code
        mats = bpy.context.active_object.data.materials
    else:
        #this is what will happen
        things = bpy.data.objects
      
    bi_gloss = 'no'
    bi_emitt = 'no'
    bi_matt = 'yes'
    #added new slot opacity
    opacity = 1
    bi_image = 'no'
    bi_3Dtex = 'nothing'
    bi_turbulence = 0.5
    bi_diffuse = 'no'
    bi_trans = 'no'
    bi_ambient = 'no'
    bi_glass = 'no'
    bi_alpha = 'no'
    bi_reflect = 'no'
    bi_bump = 'no'
    bi_normal = 'no'
    bi_specular = 'no'
    bi_lucent = 'no'
    bi_mlucent = 'no'
    bi_display = 'no'
    bi_texsize = 0.0
    bi_texscaleX = 1.0
    bi_texscaleY = 1.0
    bi_texscalez = 1.0
    lux_output = 'no'
    bi_metal = 'no'
    bi_metalx = 'no'
    bi_mirror = 'no'
    bi_skin = 'no'
    bi_dimensionX = 2
    bumpnum = 0.01
    matSlot = 0
    teximage = ''
    bumpimage = ''
    ambientimage = ''
    texambientuse = False
    texspecularuse = False
    imgalready = 'no'
    glassimgalready = 'no'
    use_shader = 0
    shaderX = 900
    shaderY = 300
    object_name = ''
    material_name = ''
    bumpfactor = 0.45
    nowmat_is_transp = False
    use_glass_image = 'no'
    ctx_img = None
    # map_glass is toggle for checkbox
    mat_depth = 0.0
    roughval = 0
    material_idx = 0
    mirror_added = 0
    metal_type = ''
    #Lux and Cycle different here (alpha):
    light_color = (0.9,0.9,0.8)
    diff_color = (0.4,0.4,0.4)
    absorb_col = (0.02, 0.02, 0.02)
    transmitt_col = (0.6, 0.6, 0.6)
    specular_col = (0.03, 0.03, 0.03)
    metalcol = (0.0, 0.0, 0.0)
    
    speccol_r = 0.03
    speccol_g = 0.03
    speccol_b = 0.03
    texnormalfact = 0.5
    j = -1
    fj = -1
    L = 0
    timestring = ''
    cstep = 0
    slotNum = 0
    alphaslot = -1
    texslot = -1
    imgslot = -1
    bumpslot = -1
    specslot = -1
    normslot = -1
    ambientslot = -1
    texalpha = 0
    pixels = [0]
    imgalpha = 'no'
    glassarch = 'no'
    power = 0
    shadernode = ''
    matspecinty = 0.0
    spec_changed = 'no'
    edge_weapon = 'no'
    matnull = 'no'
    lightalready = 'no'
    # End of startup variables
    
    runstart = time.strftime("%H%M%S")
    startnum = int(time.time())
    
    
    
    timestring = str(runstart)
    
    # Log File:
    lpath = outpath + '\log'
    try:
        os.makedirs(lpath)
    except:
        e = sys.exc_info()[0]
        if e != '':
            print ("OK, Log Folder exists: " + str(e))
            
    lpath = lpath + "//"
    wr = open(lpath + 'LuxConvert' + timestring + '.log', 'a')
    wr.write("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n")
    wr.write("New Run....\n")
    wr.write("Blender to LuxRender Converter\n")
    wr.write("  Marshall Flynn, 2016\n")
    wr.write("------------------------------------------------\n")
    wr.write("Started at " + str(runstart) + "\n")
    wr.write("Toggle Map Glass: " + str(map_glass) + "\n")
    # Start iterating objects
    # If object normals are messed up, so will the image maps.  This will not correct normals.
    for nowthing in things:
        
        if nowthing.type not in {'CAMERA', 'LAMP'}:
            object_name = str(nowthing.name)
            print ("Iterating Objects - Now on " + str(object_name))
            print ("Object Loop -----------------------------------------------")
            bi_dimensionX = nowthing.dimensions[0]
            bi_dimensionY = nowthing.dimensions[1]
            wr.write(">>>New Object --------------------------------\n")
            wr.write(">>> Supposedly: " + str(object_name) + "\n")
            
            
            #bpy.context.scene.objects.active = bpy.data.objects[str(nowthing.name)]
            # Active object
            actob = bpy.context.active_object
            print ("Object Loop Technical Active Object: " + str(actob))
            print ("Object Loop Current Object: " + str(object_name))
            print ("Object Loop Active Context Object: " + str(bpy.context.object.name)) # Not right
            try:
                wr.write("Current Object is: " + str(object_name)+ "\n")
                wr.write("Current Object X Dimension: " + str(bi_dimensionX) + "\n")
                wr.write("Current Object Type is: " + str(nowthing.type)+ "\n")
                
            except:
                wr.write("We are having Unicode issues!  Check material names....: " + "\n")
                # Maybe bail on this object?
                
                
            if nowthing.modifiers:
                # there is a modifier
                for modified in nowthing.modifiers:
                    wr.write("Current Object has this modifier: " + str(nowthing.modifiers.type)+ "\n")
            else:
                # no modifiers
                wr.write("Current Object has no modifier(s): " + "\n")
        
                
            #mats = bpy.context.active_object.data.materials
            #for nowmat in nowthing:  #  Material Loop      
            for mats in nowthing.material_slots:
                nowmat = mats.material
                bpy.context.object.active_material_index = material_idx
                print (" ")
                print ("Material Loop Current Object: " + str(object_name))
                print ("New Material ------------------------------------------------")
                print ("Raw Loop material: " + str(mats) )
                # wr.write(" Found Diffuse Color: " + str(nowmat.diffuse_color) + "\n")
                print ("Active Material Index: " + str(things[object_name].active_material_index))
                sys.stdout.flush()  #Flush annoying retained text
                
                #This maybe where material slots gets screwed up (comment out)
                #bpy.context.active_object.active_material = bpy.data.materials[str(nowmat.name)]
                wr.write("New Material ----------------------------------------\n")
                wr.write(" Current Material: " + nowmat.name + "\n")
                print ("Next 3 may not match match.......OK")
                print ("  Loop nowmat is " + nowmat.name)
                if bpy.context.object.active_material:
                   print ("  Technical Active Material is: " + str(bpy.context.active_object.active_material))
                   # print ("Technical Active Node Material is: " + str(nowmat.active_node_material))
                   wr.write(" Active Material: " + nowmat.name + "\n")
                   wr.write(" Active Node Material: " + str(nowmat.active_node_material) + "\n")
                   wr.write(" Material Slot: " + str(bpy.context.object.material_slots) + "\n")
                   wr.write(" Active Material Index: " + str(things[object_name].active_material_index) + "\n")
                idtype = 'Material'
                # context_data = bpy.context.active_object.active_material
                context_data = nowmat
                
                # nowmat.use_transparency = False        
                # print ("FEEDBACK: Active Material BI is: " + context_data.name)
                
                
                
                material_name = nowmat.name
                # Grab the existing material from Blender Internal
                matambient = nowmat.ambient
                matalpha = nowmat.alpha
                matdcolor = nowmat.diffuse_color
                wr.write(" Original Diffuse Color (variable): " + str(matdcolor) + "\n")
                #print ("Test: Diffuse = " +str(nowmat.diffuse_color ))
                #print ("End Test....") 
                for primary in matdcolor:
                    cstep = cstep + 1
                    if cstep == 1:
                      dcolor_r = primary
                    if cstep == 2:
                      dcolor_g = primary
                    if cstep == 3:
                      dcolor_b = primary
                    if cstep == 4:
                      dcolor_a = primary
                      
                print ("Here is Material Color (matdcolor): " + str(matdcolor))
                
                wr.write(" Check color (after dcolor changes): " + str(nowmat.diffuse_color) + "\n")
                wr.write(" OK. Check actual material: " + str(nowmat) + "\n")
                print ("Lets print the red of that: " + str(matdcolor.r))
                matdiffuseinty = nowmat.diffuse_intensity
                matemitt = nowmat.emit
                matmirrorcol = nowmat.mirror_color
                matRTmirror = nowmat.raytrace_mirror.reflect_factor
                matuseRTmirror = nowmat.raytrace_mirror.use
                matRTtrans = nowmat.raytrace_transparency
                matspeccol = nowmat.specular_color
                matspecinty = nowmat.specular_intensity
                matspechard = nowmat.specular_hardness
                matusetrans = nowmat.use_transparency
                mattranstype = nowmat.transparency_method
                mattransIOR = nowmat.raytrace_transparency.ior
                # print ("test: " + str(matspeccol.r))
                wr.write(" Mat Specular Color orig: " + str(nowmat.specular_color) + "\n")
                if matspeccol.r > 0.3 and matspeccol.g > 0.3:
                    if matspeccol.b > 0.3:
                        #matspeccol = specular_col
                        matspeccol.r = 0.032
                        matspeccol.g = 0.032
                        matspeccol.b = 0.032
                        spec_changed = 'yes'
                wr.write(" matdcolor.r: " + str(matdcolor.r) + "\n")        
                if matdcolor.r > 0.9 and matdcolor.g > 0.9:
                    if matdcolor.b > 0.9:
                        
                        matspeccol.r = 0.052
                        matspeccol.g = 0.052
                        matspeccol.b = 0.052
                        
                wr.write(" Mat Specular Color now: " + str(nowmat.specular_color) + "\n")
                wr.write(" Mat Diffuse Color now: " + str(nowmat.diffuse_color) + "\n")
                # Texture Properties
                teximgpath = ''
                texglasspath = ''
                texspecpath = ''
                texbumppath = ''
                texnormpath = ''
                texalphapath = ''
                for tex in nowmat.texture_slots:
                    j = j + 1
                    if tex:
                        print ("Tex Type: " + tex.texture.type)
                        print ("Original texture coordinates: " + str(tex.texture_coords))
                        wr.write(" New Texture Slot ................................. " + "\n")
                        wr.write(" Texture Type: " + str(tex.texture.type) + "\n")
                        wr.write(" Original texture coordinates: " + str(tex.texture_coords) + "\n")
                        #Iterate through textures
                        
                        if tex.texture.type == 'IMAGE':
                            
                            
                            print("FEEDBACK:  Found IMAGE shader node " + tex.texture.name)
                            print ("FEEDBACK:  Current image slot: " + str(j))
                            wr.write(" Texture Image: " + tex.texture.name + "\n")
                            bi_image = 'yes'
                            print ("Check-bi_glass: " + bi_glass)
                            
                            texreflectfact = tex.reflection_factor
                            texreflectuse = tex.use_map_reflect
                            texmap = tex.mapping
                            texemittuse = tex.use_map_emit
                            texdisplaceuse = tex.use_map_displacement
                            texnormaluse = tex.use_map_normal
                            texnormalfact = tex.normal_factor
                            texdisplaceuse = tex.use_map_displacement
                            texdiffuseuse = tex.use_map_diffuse
                            texdiffusemapcol = tex.use_map_color_diffuse
                            texambientuse = tex.use_map_ambient
                            texambientfact = tex.ambient_factor
                            texalphause = tex.use_map_alpha
                            texspecularuse = tex.use_map_specular
                            texalphafact = tex.alpha_factor
                            texspecularfact = tex.specular_factor
                            texcoords = tex.texture_coords
                            Bsub = teximage[0:4]
                            texslot = j
                            teximage = tex.texture.name
                            print ("FEEDBACK:  Credible Texture Image Info: " + str(tex.texture.image))
                            wr.write(" What is this Image Texture Slot: " + str(tex.texture.image) + " on " + str(texslot) + "\n")
                            wr.write(" Texture Mapping: " + str(texmap) + " on " + str(texslot) + "\n")
                            
                            teximgrealname = tex.texture.image.name
                            #  There may not be an image in slot - now we check:
                            if tex.texture.image:
                                    
                                texpath = tex.texture.image.filepath
                                if texdiffuseuse == True or texdiffusemapcol == True:
                                    bi_diffuse = 'yes'
                                    bi_specular = 'no'
                                    bi_ambient = 'no'
                                    
                                    teximgrealname = tex.texture.image.name
                                    teximgpath = tex.texture.image.filepath
                                    #Try to check if tranparent parts.......
                                    iwidth, iheight = tex.texture.image.size
                                    print (" texture image size (w,h): " + str(iwidth) + ", " + str(iheight))
                                    wr.write(" texture image size (w,h): " + str(iwidth) + ", " + str(iheight) + "\n")
                                    # pixels = tex.texture.image.pixels[:]
                                    iy = iheight/2
                                    ix = iwidth/2
                                    block_number = (iy * iwidth) + ix
                                    alpha = teximgpath[-3:]
                                    if alpha.lower() == 'png':
                                        imgalpha = 'yes'
                                        wr.write(" Uses Alpha Image (transparent): " + imgalpha + "\n")
                                    

                                    mapimage = teximage
                                    imgslot = texslot
                                    print ("Image is alpha: " + imgalpha)
                                    wr.write(" Uses Diffuse Image Mapping: " + bi_diffuse + "\n")
                                    
                                
                                if "glass" in nowmat.name.lower() and use_glass_image == 'yes':
                                    bi_glass = 'yes'
                                    
                                    #use_glass_image = 'yes'
                                    bi_diffuse = 'no'
                                    nowmat.use_transparency = True
                                    texglassrealname = tex.texture.image.name
                                    texglasspath = tex.texture.image.filepath
                                    alphaslot = texslot
                                    wr.write(" Uses Glass Image Mapping from name: " + bi_diffuse + "\n")
                                
                                if 'D' in tex.texture.name and not 'Diffuse' in tex.texture.name:
                                    # The capital 'D' is from an OBJ file import
                                    if use_glass_image == 'no':
                                        print ("Here is where we search: " + str(teximage))
                                        print ("we found a D in here...")
                                        
                                        if "bottle" in nowmat.name.lower():
                                            bi_diffuse = 'no'
                                        
                                        if bi_diffuse == 'yes' and texalphause == True:
                                            # bi_diffuse = 'yes'
                                            bi_glass = 'no'
                                            bi_alpha = 'yes'
                                            alphaslot = imgslot
                                            wr.write(" Uses Alpha mapping because Diffuse map exists: " + bi_alpha + "\n")
                                            
                                            
                                        else:
                                            # It is possible to use alpha without a diffuse image
                                            texglasspath = tex.texture.image.filepath
                                            texglassrealname = tex.texture.image.name
                                            if matdcolor.r < 0.7:
                                                #possible diffuse color and NOT glass
                                                bi_glass = 'no'
                                                bi_image = 'yes'
                                                bi_alpha = 'yes'
                                                alphaslot = texslot
                                                wr.write(" Tex Slot - We think this is an alpha map " + "\n")
                                                wr.write( nowmat.texture_slots[alphaslot].texture.image.name+ "\n")
                                            else:
                                                bi_glass = 'yes'
                                                #mapimage = teximage
                                                bi_image = 'no'
                                                bi_diffuse = 'no'
                                                #alphaslot = imgslot
                                                use_glass_image = 'no'
                                                wr.write(" Nixxed the image map because use_glass_image is no. " + "\n")
                                        
                                        nowmat.use_transparency = True
                                        
                                        if bi_alpha == 'no':
                                            print ("also Glass Image location: " + texglasspath)
                                            print ("Check-bi_glass: " + bi_glass)
                                            wr.write(" Tex Slot - Uses Glass Image Mapping: " + use_glass_image + "\n")
                                            wr.write("   would have used this image name: " + teximage + "\n")
                                    else:
                                        print ("already glass mapped")
                                if texnormaluse == True or texdisplaceuse == True:
                                    # A bump type of image
                                    texbumppath = tex.texture.image.filepath
                                    texbumprealname = tex.texture.image.name
                                    bi_bump = 'yes'
                                    #mapimage = teximage
                                    bumpimage = tex.texture.name
                                    print ("Changed to bump due to slot normaluse: " + bi_bump)
                                    wr.write(" Changed to Bump because Normal yes or Displace yes: " + bi_bump + "\n")
                                    bumpslot = texslot
                                if texnormaluse == True:
                                    if texnormalfact > 0.0 and texdisplaceuse == True:
                                        # A normal type of image
                                        # This could undo a bump image though, but if bump in name, then restored below
                                        texnormpath = tex.texture.image.filepath
                                        texnormrealname = tex.texture.image.name
                                        bi_normal = 'yes'
                                        bi_bump = 'no'
                                        normslot = texslot
                                        normimage = tex.texture.name
                                        if texnormalfact == 1.0:
                                            texnormalfact = 0.32
                                            wr.write(" Texture normal was 1.0, so changed: " + str(texnormalfact) + "\n")
                                        print ("Changed to Normal only due to Displacement = " + str(texdisplaceuse))
                                        wr.write(" Changed to Normal only because Normal yes and Displace No: " + bi_bump + "\n")
                                if 'bump' in tex.texture.name.lower():
                                    # If the name is 'Bump', it was an imported OBJ file
                                    texbumppath = tex.texture.image.filepath
                                    texbumprealname = tex.texture.image.name
                                    bi_bump = 'yes'
                                    bi_normal = 'no'
                                    texnormaluse = False
                                    texnormpath = "nothing"
                                    # mapimage = teximage
                                    bumpimage = tex.texture.name
                                    #overrides normal
                                    print ("Changed to bump due to word Bump: " + bi_bump)
                                    wr.write(" Changed because of word Bump: " + bi_bump + "\n")
                                    bumpslot = texslot
                                
                                if texspecularuse == True:
                                    # A specular type of image
                                    texspecpath = tex.texture.image.filepath
                                    texspecrealname = tex.texture.image.name
                                    bi_specular = 'yes'
                                    bi_diffuse = 'no'
                                    bi_ambient = 'no'
                                    specslot = texslot
                                    specimage = tex.texture.name
                                if texambientuse == True:
                                    # An ambient type of image
                                    texambipath = tex.texture.image.filepath
                                    texambirealname = tex.texture.image.name
                                    bi_specular = 'no'
                                    bi_diffuse = 'no'
                                    bi_ambient = 'yes'
                                    ambientslot = texslot
                                    ambientimage = tex.texture.name
                                if texalphause == True:
                                    # could be redundant check......
                                    if bi_diffuse == 'no':
                                        if use_glass_image == 'yes':
                                            # A glass type of image
                                            print ("Check-bi_glass [use_alpha]: " + bi_glass)
                                            texglasspath = tex.texture.image.filepath
                                            texglassrealname = tex.texture.image.name
                                            bi_glass = 'yes'
                                            bi_diffuse = 'no'
                                            bi_alpha = 'no'
                                            alphaimage = tex.texture.name
                                            nowmat.use_transparency = True
                                            use_glass_image = 'yes'
                                            alphaslot = texslot
                                            print ("also Glass Image location: " + texglasspath)
                                            wr.write(" Tex Slot Alpha - Uses Glass Image Mapping: " + use_glass_image + "\n")
                                        else:
                                            print ("Glass already mapped")
                                    else:
                                        # A plain alpha type of image
                                        print ("Check-bi_glass [use_alpha]: " + bi_glass)
                                        print ("Most likely a Light Map " )
                                        
                                        alphaimage = tex.texture.name
                                        texalphapath = tex.texture.image.filepath
                                        texalpharealname = tex.texture.image.name
                                        bi_alpha = 'yes'
                                        bi_glass = 'no'
                                        alphaslot = texslot
                                        wr.write(" Tex Slot Alpha - Uses Alpha Image Mapping " + "\n")
                                
                                
                                    
                                
                                   
                                wr.write(" TEXTURE SLOT [" + str(j) + "] DETAILS: " + "\n")
                                print ("This material has color image: " + teximage)
                                print ("  The image file name: " + teximgrealname)
                                wr.write(" Image Filename: " + teximgrealname + "\n")
                                print ("  Current File location: " + texpath)
                                wr.write(" Current Image Location: " + texpath + "\n")
                                print ("  Used for diffuse: " + str(texdiffuseuse))
                                print ("  Used for diffuse map: " + str(texdiffusemapcol))
                                wr.write(" For Diffuse Map: " + str(texdiffuseuse) + "\n")
                                wr.write(" For Diffuse Map Color: " + str(texdiffusemapcol) + "\n")
                                print ("  Used for normal: " + str(texnormaluse))
                                wr.write(" For Normal: " + str(texnormaluse) + "\n")
                                print ("  Used for displacement: " + str(texdisplaceuse))
                                print ("  Used for specular: " + str(texspecularuse))
                                print ("  Used for alpha: " + str(texalphause))
                                wr.write(" For Displacement: " + str(texdisplaceuse) + "\n")
                                wr.write(" For Alpha: " + str(texalphause) + "\n")
                                print ("  Used for ambient: " + str(texambientuse))
                                wr.write(" For Ambient: " + str(texambientuse) + "\n")
                                wr.write(" For Specular: " + str(texspecularuse) + "\n")
                                print ("  Image mapping: " + str(texmap))
                                wr.write(" Image Mapping: " + str(texmap) + "\n")
                                wr.write(" Image Mirror Value: " + str(texreflectfact) + "\n")
                                wr.write(" Image Ambient Value: " + str(texambientfact) + "\n")
                                wr.write(" Image Normal Value: " + str(texnormalfact) + "\n")
                                wr.write(" Image Alpha Value: " + str(texalphafact) + "\n")
                                print ("  Texture Blend Type: " + str(tex.blend_type))
                                wr.write(" Textures final - Uses Glass Image Mapping: " + use_glass_image + "\n")
                                wr.write(" End this Texture ________________________________ " + "\n")
                                
                            else:
                                # No image or pathname
                                print ("Image slot has no actual image")
                                
                                bi_image = 'no'
                                bi_bump = 'no'
                                bi_diffuse = 'no'
                                bi_specular = 'no'
                                bi_ambient = 'no'
                                wr.write(" texture Slot has no actual image or path!: " + "\n")
                        if tex.texture.type == 'VORONOI':
                            print ("texture is Voronoi!")
                            print ("Normal: " + str(tex.use_map_normal))
                            print ("Normal Factor: " + str(tex.normal_factor))
                            print ("Blend Type: " + str(tex.blend_type))
                            bi_3Dtex = tex.texture.type
                            texnormaluse = tex.use_map_normal
                            texnormalfact = tex.normal_factor
                            bi_texsize = tex.texture.noise_scale
                            bi_texscaleX = (tex.scale[0])
                            bi_texscaleY = (tex.scale[1])
                            bi_texscaleZ = (tex.scale[2])
                        print ("Texture found is " +  str(tex.texture.type))  
                        if tex.texture.type == 'STUCCI':
                            print ("texture is Stucci!")
                            print ("Normal: " + str(tex.use_map_normal))
                            print ("Normal Factor: " + str(tex.normal_factor))
                            print ("Blend Type: " + str(tex.blend_type))
                            bi_3Dtex = tex.texture.type
                            bi_turbulence = tex.turbulence
                            texnormaluse = tex.use_map_normal
                            texnormalfact = tex.normal_factor
                            bi_texsize = tex.texture.noise_scale
                            bi_texscaleX = (tex.scale[0])
                            bi_texscaleY = (tex.scale[1])
                            bi_texscaleZ = (tex.scale[2])
                        if tex.texture.type == 'DISTORTED_NOISE':
                            print ("texture is Dis Noise!")
                            print ("Normal: " + str(tex.use_map_normal))
                            print ("Normal Factor: " + str(tex.normal_factor))
                            print ("Blend Type: " + str(tex.blend_type))
                            bi_3Dtex = tex.texture.type
                            texnormaluse = tex.use_map_normal
                            texnormalfact = tex.normal_factor
                            bi_texsize = tex.texture.noise_scale
                            bi_texscaleX = (tex.scale[0])
                            bi_texscaleY = (tex.scale[1])
                            bi_texscaleZ = (tex.scale[2])
                        if tex.texture.type == 'CLOUDS':
                            print ("texture is Clouds!")
                            print ("Normal: " + str(tex.use_map_normal))
                            print ("Normal Factor: " + str(tex.normal_factor))
                            print ("Blend Type: " + str(tex.blend_type))
                            bi_3Dtex = tex.texture.type
                            texnormaluse = tex.use_map_normal
                            texnormalfact = tex.normal_factor
                            bi_texsize = tex.texture.noise_scale
                            bi_texscaleX = (tex.scale[0])
                            bi_texscaleY = (tex.scale[1])
                            bi_texscaleZ = (tex.scale[2])
                        fj = j
                    else:
                        wr.write(" Texture Slot " + str(j) + " is empty" + "\n")
                    
                # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++           
                print ("Material Information -------------------------------")
                print ("Final J: " + str(j))
                print ("Final FJ: " + str(fj))
                wr.write(" Material Data ---------------------------------------- " + "\n")
                print ("Material name: " + nowmat.name)
                print ("Original Material Alpha setting: " + str(nowmat.alpha))
                wr.write("  Original Material Alpha: " + str(matalpha) + "\n")
                print ("Original Diffuse Red setting: " + str(dcolor_r))
                print ("Original Diffuse Green setting: " + str(dcolor_g))
                print ("Original Diffuse Blue setting: " + str(dcolor_b))
                #print ("Original Diffuse Alpha setting: " + str(dcolor_a))
                print ("Original Specular Color: " + str(matspeccol))
                wr.write("  Current Specular Color: " + str(matspeccol) + "\n")
                wr.write("  Current Specular Intensity: " + str(matspecinty) + "\n")
                wr.write("  Specular Color lowered: " + str(spec_changed) + "\n")
                print ("Original Diffuse Color: " + str(nowmat.diffuse_color))
                wr.write("  Original Red Color: " + str(dcolor_r) + "\n")
                wr.write("  Original Green Color: " + str(dcolor_g) + "\n")
                wr.write("  Original Blue Color: " + str(dcolor_b) + "\n")
                wr.write("  Original Diffuse Color: " + str(matdcolor) + "\n")
                print ("Original Ambient setting: " + str(nowmat.ambient))
                wr.write("  Original Ambient: " + str(nowmat.ambient) + "\n")
                print ("Originally Transparent: " + str(nowmat.use_transparency))
                wr.write("  Originally set as transparent: " + str(matusetrans) + "\n")
                wr.write("  Originally set as Mirror: " + str(matuseRTmirror) + "\n")
                wr.write("  Original Mirror data: " + str(matRTmirror) + "\n")
                print ("Originally Transparent Type: " + str(mattranstype))
                wr.write("  Original transparent type: " + str(mattranstype) + "\n")
                print ("Originally Reflection: " + str(matuseRTmirror))
                print ("Originally Emitting: " + str(matemitt))
                wr.write("  Originally emitting or glowing: " + str(matemitt) + "\n")
                print ("Original IOR setting: " + str(mattransIOR))
                wr.write("  Original Trans IOR: " + str(mattransIOR) + "\n")
                
                wr.write("  Original Specular Intensity: " + str(matspecinty) + "\n")
                print ("Diffuse Image Path: " + str(teximgpath))
                print ("Glass Image Path: " + str(texglasspath))
                print ("Specular Image Path: " + str(texspecpath))
                print ("Bump Image Path: " + str(texbumppath))
                print ("Normal Image Path: " + str(texnormpath))
                print ("Alpha Image Path: " + str(texalphapath))
                print ("If 3D Texture: " + str(bi_3Dtex))
                wr.write("  If 3D Texture: " + str(bi_3Dtex) + "\n")
                wr.write("  Material String Variable is " + str(material_name) + "\n")
                # Start with something
                
                bi_matt = 'yes'
                
                
                if bi_glass == 'yes':
                    print ("Check-bi_glass: " + bi_glass)
                    bi_matt = 'no'
                    if matspecinty == 0.0:
                        nowmat.specular_intensity = 1.0
                    print ("Glass change using OBJ texture slot!")
                    wr.write("  Glass changes opacity to 50%" + "\n")
                    opacity = 0.5
                    wr.write("  Glass change using OBJ texture slot" + "\n")
                    matdcolor = (0.7, 0.7, 0.7)
                if bi_glass == 'yes' and use_glass_image == 'no':
                    bi_image = 'no'
                    bi_alpha = 'no'
                    matdcolor = (1.2 * dcolor_r,1.2 * dcolor_g,1.2 * dcolor_b)
                    
                    if matdcolor[0] > 1 and matdcolor[1] > 1:
                        matdcolor = (0.9,0.9,0.9)
                    wr.write("  Material or Transmission Color is " + str(matdcolor) + "\n")    
                if matusetrans == True:
                    nowmat_is_transp = True
                    if matspecinty > 0.75 and matalpha < 0.31:
                        bi_glass = 'yes'
                        bi_matt = 'no'
                        bi_lucent = 'no'
                        print ("Glass change using original Transparency!")
                        wr.write("  Glass change due to original Transparency - be careful!" + "\n")
                    if matalpha > 0.31 and matalpha < 0.9:
                        bi_glass = 'no'
                        bi_matt = 'no'
                        bi_lucent = 'yes'
                        wr.write("  Original Use_Transparency, but Alpha low - so now Translucent" + "\n")
                    if matalpha >= 0.9 and use_glass_image == 'no':
                        bi_glass = 'no'
                        bi_matt = 'yes'
                        bi_lucent = 'no'
                        bi_mlucent = 'no'
                        wr.write("  Original Use_Transparency, but Alpha high - so now Matte" + "\n")
                    
                    # not always true. OBJ importer has all material transparent
                if nowmat.alpha > 0.0 and nowmat.alpha < 0.4:
                    nowmat_is_transp = True
                    bi_glass = 'yes'
                    bi_gloss = 'no'
                    bi_matt = 'no'
                    print ("Glass change using Alpha!")
                    wr.write("  Glass change due to Alpha low value" + "\n")
                if matuseRTmirror == True and matRTmirror > 0.9:
                    bi_mirror = 'yes'
                    bi_gloss = 'no'
                    bi_matt = 'no'
                    bi_lucent = 'no'
                    bi_mlucent = 'no'
                    print ("Mirror change!")
                    wr.write("  Mirror change due to Raytrace Mirror and reflection" + "\n")
                if matuseRTmirror == True and matRTmirror > 0.1:
                    if matalpha >= 0.4:
                        bi_mirror = 'no'
                        bi_gloss = 'yes'
                        bi_matt = 'no'
                        bi_lucent = 'no'
                        bi_mlucent = 'no'
                        bi_glass = 'no'
                        print ("Gloss change!")
                        wr.write("  Gloss change due to Raytrace Mirror and reflection" + "\n")
                if matRTmirror >= 0.1 and matRTmirror < 0.9:
                    if matalpha > 0.0 and matalpha < 0.4:
                        bi_mirror = 'no'
                        bi_gloss = 'no'
                        bi_matt = 'no'
                        bi_lucent = 'no'
                        bi_mlucent = 'no'
                        bi_glass = 'yes'
                        
                        print ("Glass change!")
                        wr.write("  Glass change due to Raytrace Mirror and Alpha" + "\n")
                if nowmat.specular_color.r == 1.0 and nowmat.specular_color.g == 1.0:
                    if nowmat.specular_color.b == 1.0:
                        #this could definitely be glass or could be bad import
                        if matusetrans == 'yes':
                            bi_glass = 'yes'
                            wr.write("  Possible Glass due to Specular all 1.0" + "\n")
                if nowmat.transparency_method == 'Z_TRANSPARENCY':
                    if nowmat.alpha > 0.0 and nowmat.alpha < 0.4:
                        nowmat_is_transp = True
                        bi_glass = 'yes'
                        bi_lucent = 'no'
                        bi_mlucent = 'no'
                        bi_matt = 'no'
                        print ("Glass change due to Z & Alpha!")
                        wr.write("  Glass change due to Z Transparency & Alpha" + "\n")
                    if nowmat.alpha >= 0.6 and nowmat.alpha < 0.9:
                        if bi_gloss == 'yes':
                            bi_lucent = 'yes'
                            bi_gloss = 'no'
                            bi_matt = 'no'
                        else:
                            if bi_alpha == 'no':
                                bi_mlucent = 'yes'
                if nowmat.transparency_method == 'RAYTRACE':
                    if nowmat.alpha > 0.0 and nowmat.alpha < 0.4:
                        nowmat_is_transp = True
                        bi_glass = 'yes'
                        bi_matt = 'no'
                        print ("Glass change due to Z & Alpha!")
                        wr.write("  Glass change due to Z Transparency & Alpha" + "\n")
                    if nowmat.alpha >= 0.6 and nowmat.alpha < 0.9:
                        if bi_gloss == 'yes':
                            bi_lucent = 'yes'
                            bi_gloss = 'no'
                        else:
                            bi_mlucent = 'yes'
                if nowmat.use_transparency == True and nowmat.alpha > 0.99:
                    # Corrects OBJ import anomaly
                    if use_glass_image == 'no':
                       bi_glass = 'no'
                       bi_gloss = 'no'
                       bi_matt = 'yes'
                       print ("Glass remove as OBJ anomaly!")
                       wr.write("  Remove OBJ opaque alpha anomaly " + "\n")
                if matemitt > 0.0:
                    bi_emitt = 'yes'
                    
                    
        
        
                
                # Following are Material Overrides
                wr.write("  * GUI Checkbox status [map_glass]: " + map_glass + "\n")
                wr.write("  Material Overrides: " + "\n")
                print ("Material Overrides here......")
                skin_depth = 5 * (bi_dimensionX * 0.85)
                wr.write("  bi dimension is : " + str(bi_dimensionX) + "\n")
                # Check if glass will contain image map
                wr.write("  Material Name check: " + str(nowmat.name.lower()) + "\n")
                wr.write("  Skin Depth would be: " + str(skin_depth) + "\n")
                if map_glass == 'no':
                    use_glass_image = 'no'
                    wr.write("  Removed Glass Image due to GUI checkbox" + "\n")
                if "glass" in nowmat.name.lower():
                    if "wood" in nowmat.name.lower():
                        #do nothing
                        print ("Wood in name")
                    else:
                        bi_glass = 'yes'
                        bi_matt = 'no'
                        bi_gloss = 'no'
                        bi_lucent = 'no'
                        bi_mlucent = 'no'
                        transmitt_col = (0.92, 0.92, 0.92)
                        roughval = 0
                        wr.write("  Converted to Glass due to Material Name Glass" + "\n")
                if "flame" in nowmat.name.lower():
                    bi_emitt = 'yes'
                    bi_mlucent = 'yes'
                    bi_lucent = 'no'
                    bi_matt = 'no'
                    bi_glass = 'no'
                    mat_depth = 0.6
                    # Careful with renderer (alpha):
                    light_color = (0.8,0.8,0.5)
                    diff_color = matdcolor
                    transmitt_col = (0.5, 0.5, 0.2)
                    power = 60
                    wr.write("  Converted to Emitt due to Material Name Flame" + "\n")
                    lightalready = 'yes'
                    
                if "display" in nowmat.name.lower():
                    bi_emitt = 'no'
                    bi_mlucent = 'no'
                    bi_lucent = 'no'
                    bi_matt = 'yes'
                    bi_glass = 'no'
                    bi_display = 'yes'
                    
                    # Careful with renderer (alpha):
                    light_color = (0.8,0.8,0.5)
                    diff_color = matdcolor
                    transmitt_col = (0.5, 0.5, 0.5)
                    power = 75
                    wr.write("  Converted to Display due to Material Name Display" + "\n")
                
                if "bulb" in nowmat.name.lower():
                    bi_emitt = 'yes'
                    bi_mlucent = 'no'
                    bi_lucent = 'no'
                    bi_matt = 'no'
                    bi_glass = 'yes'
                    mat_depth = 0.3
                    roughval = 0.01
                    light_color = (0.9,0.9,0.8)
                    diff_color = matdcolor
                    power = 70
                    wr.write("  Converted to Emitt due to Material Name Lightbulb" + "\n")
                    lightalready = 'yes'
                
                if "lightbulb" in nowmat.name.lower():
                    bi_emitt = 'yes'
                    bi_mlucent = 'no'
                    bi_matt = 'no'
                    bi_glass = 'yes'
                    mat_depth = 0.3
                    roughval = 0.01
                    light_color = (0.9,0.9,0.8)
                    diff_color = matdcolor
                    power = 70
                    wr.write("  Converted to Emitt due to Material Name Lightbulb" + "\n")
                    lightalready = 'yes'
                if "glow" in nowmat.name.lower():
                    # represent some emitting object  assume fairly translucent
                    bi_mlucent = 'yes'
                    bi_emitt = 'yes'
                    opacity = 0.6
                    mat_depth = 0.4
                    power = 0.90
                    wr.write("  Converted to Emitt due to Material Name has glow" + "\n")
                    lightalready = 'yes'
                if "light" in nowmat.name.lower() and lightalready == 'no':
                    if "bulb" not in nowmat.name.lower():
                        if "blue" not in nowmat.name.lower() or "red" not in nowmat.name.lower():
                            if "green" not in nowmat.name.lower():
                                if "stone" not in nowmat.name.lower():
                                    if dcolor_r < 0.4 and dcolor_g < 0.4:
                                        if bi_matt == 'yes' or bi_metal == 'yes':
                                             bi_emitt = 'no'
                                             bi_gloss = 'yes'
                                             bi_mlucent = 'no'
                                    else:
                                        bi_emitt = 'yes'
                                        bi_mlucent = 'yes'
                                        bi_matt = 'no'
                                        bi_glass = 'no'
                                        mat_depth = 0.9
                                        roughval = 0.09
                                        light_color = (0.7,0.7,0.5)
                                        diff_color = matdcolor
                                        if power == 0:
                                            power = 60
                                        wr.write("  Converted to Emitt due to Material Name Light or Lights" + "\n")
                    
                
                if bi_glass == 'no' and bi_diffuse == 'no':
                                        
                    if "window" in nowmat.name.lower():
                       bi_glass = 'yes'
                       bi_emitt = 'no'
                       glassarch = 'yes'
                       roughval = 0.005
                       transmitt_col = (0.9, 0.9, 0.9)
                       wr.write("  Converted to Glass due to Material Name Window" + "\n")
                    if "win" in nowmat.name.lower() and "window" not in nowmat.name.lower():
                       bi_glass = 'yes'
                       bi_emitt = 'no'
                       glassarch = 'yes'
                       roughval = 0.005
                       transmitt_col = (0.9, 0.9, 0.9)
                       wr.write("  Converted to Glass due to Material Name Window" + "\n")
                    
                if "tear" in nowmat.name.lower():
                    bi_glass = 'yes'
                    roughval = 0.002
                    transmitt_col = (0.9, 0.9, 0.9)
                    wr.write("  Converted to Glass due to Material Name Tear" + "\n")
                if "cornea" in nowmat.name.lower():
                    bi_glass = 'yes'
                    bi_matt = 'no'
                    bi_image = 'no'
                    glassarch = 'yes'
                    roughval = 0.001
                    transmitt_col = (0.9, 0.9, 0.9)
                    opacity = 0.7
                    wr.write("  Converted to Glass due to Material Name Cornea (override)" + "\n")
                if "iris" in nowmat.name.lower():
                    bi_glass = 'no'
                    bi_matt = 'no'
                    absorb_col = (0.01, 0.01, 0.01)
                    bi_gloss = 'yes'
                    roughval = 0.002
                    wr.write("  Converted to Glossy due to Material Name iris (override)" + "\n")
                if "pupil" in nowmat.name.lower():
                    bi_glass = 'no'
                    bi_matt = 'yes'
                    bi_image = 'no'
                    absorb_col = (0.01, 0.01, 0.01)
                    roughval = 0.25
                    specular_col = (0.001, 0.001, 0.001)
                    wr.write("  Converted to Matte due to Material Name pupil (override)" + "\n")
                if "eyereflection" in nowmat.name.lower():
                    bi_glass = 'no'
                    bi_matt = 'no'
                    glassarch = 'no'
                    bi_image = 'no'
                    bi_lucent = 'no'
                    bi_gloss = 'no'
                    matnull = 'yes'
                    wr.write("  Converted to nothing due to Material Name Eyereflection" + "\n")
                if "eye" in nowmat.name.lower() and "moisture" in nowmat.name.lower():
                    bi_glass = 'no'
                    bi_matt = 'no'
                    glassarch = 'no'
                    bi_image = 'no'
                    bi_lucent = 'no'
                    bi_gloss = 'no'
                    matnull = 'yes'
                    wr.write("  Converted to nothing due to Material Name Eye Moisture" + "\n")    
                if "clear" in nowmat.name.lower():
                    bi_glass = 'yes'
                    bi_matt = 'no'
                    roughval = 0.0001
                    transmitt_col = (0.99, 0.99, 0.99)
                    wr.write("  Converted to Glass due to Material Name Clear" + "\n")
                if "mirror" in nowmat.name.lower():
                    if "wood" in nowmat.name or "Wood" in nowmat.name:
                        #do nothing
                        print ("Wood in name")
                    else:
                        bi_glass = 'no'
                        bi_gloss = 'no'
                        bi_mirror = 'yes'
                        bi_matt = 'no'
                        roughval = 0.0001
                        mirror_added = 1
                        wr.write("  Converted to Mirror due to Material Name Mirror" + "\n")
                
                if "leather" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    bi_matt = 'no'
                    bi_gloss = 'no'
                    diff_color = (0.4,0.3,0.1)
                    absorb_col = (0.04, 0.04, 0.07)
                    texnormalfact = 0.4
                    roughval = 0.32
                    mat_depth = skin_depth
                    wr.write("  Converted to Translucent due to Material Name Leather" + "\n")
                if "skin" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    bi_matt = 'no'
                    mat_depth = skin_depth
                    texnormalfact = 0.14
                    roughval = 0.225
                    bi_skin = 'yes'
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    wr.write("  Converted to Translucent due to Material Name Skin" + "\n")
                if "eye" in nowmat.name.lower():
                    roughval = 0.022
                if "hair" in nowmat.name.lower() or "bangs" in nowmat.name.lower():
                    bumpfactor = 3
                    roughval = 0.14
                    
                if "wisps" in nowmat.name.lower():
                    bumpfactor = 3
                    roughval = 0.14
                if "lips" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    roughval = 0.19
                    mat_depth = skin_depth
                    bi_skin = 'yes'
                
                if "head" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    bi_glass = 'no'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Head" + "\n")
                if "shoulder" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    bi_glass = 'no'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name shoulder" + "\n")
                if "breast" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    bi_glass = 'no'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name breast" + "\n")    
                if "torso" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Torso" + "\n")
                if "limbs" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name limbs" + "\n")
                if "arms" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Arms" + "\n")
                if "fingers" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name fingers" + "\n")
                if "feet" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name feet" + "\n")
                   
                if "arms" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Arms" + "\n")
                if "toes" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name toes" + "\n")
                if "hands" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Arms" + "\n")
                if "face" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Face" + "\n")
                if "teeth" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = 0.03
                    bi_matt = 'no'
                    bi_skin = 'no'
                    wr.write("  Converted to Translucent due to Material Name Teeth" + "\n")
                if "neck" in nowmat.name.lower():
                    bi_lucent = 'yes'
                    mat_depth = skin_depth
                    matspeccol.r = 0.03
                    matspeccol.g = 0.025
                    matspeccol.b = 0.025
                    bi_skin = 'yes'
                    wr.write("  Converted to Translucent due to Material Name Neck" + "\n")
                if "eyelash" in nowmat.name.lower():
                    bi_lucent = 'no'
                    if bi_glass == 'yes':
                        bi_glass = 'no'
                        bi_matt = 'yes'
                        bi_alpha = 'yes'
                    bi_metal = 'no'
                    mat_depth = 0
                    bi_bump = 'no'
                    bi_skin = 'no'
                    if dcolor_r == 0.0 and dcolor_g == 0.0:
                        matdcolor = (0.014, 0.014, 0.008)
                        
                    wr.write("  Converted to Matte due to Material Name Eyelash" + "\n")
                if "porcelain" in nowmat.name.lower():
                    bi_matt = 'no'
                    bi_lucent = 'yes'
                    mat_depth = 0.27
                    roughval = 0.37
                    transmitt_col = (0.070, 0.070, 0.070)
                    wr.write("  Converted to not Matte due to Porcelain in name" + "\n")
                if "clay" in nowmat.name.lower():
                    bi_matt = 'yes'
                    roughval = 0.341
                    wr.write("  Converted to low lustre due to Clay in name" + "\n")
                
                if "rubber" in nowmat.name.lower():
                    bi_gloss = 'yes'
                    roughval = 0.270
                    wr.write("  Converted to rubber due to Material Name rubber" + "\n")
                
                if "gold" in material_name.lower():
                    roughval = 0.07
                    bi_metal = 'yes'
                    bi_matt = 'no'
                    metal_type = 'gold' # has to be small letters
                if "silver" in material_name.lower():
                    roughval = 0.07
                    bi_metal = 'yes'
                    bi_matt = 'no'
                    metal_type = 'silver' # has to be small letters
                if "copper" in material_name.lower():
                    roughval = 0.07
                    bi_metal = 'yes'
                    bi_matt = 'no'
                    metal_type = 'copper' # has to be small letters
                if "pewter" in material_name.lower():
                    roughval = 0.17
                    bi_metal = 'yes'
                    bi_matt = 'no'
                    metal_type = 'pewter' # has to be small letters
                if "steel" in material_name.lower():
                    roughval = 0.15
                    bi_metal = 'yes'
                    metalcol = (0.25, 0.25, 0.25)
                    bi_matt = 'no'
                    metal_type = 'fresnelcolor' # has to be small letters
                if "iron" in material_name.lower():
                    roughval = 0.20
                    bi_metal = 'yes'
                    metalcol = (0.11, 0.11, 0.11)
                    bi_matt = 'no'
                    metal_type = 'fresnelcolor' # has to be small letters
                if "sword" in nowmat.name.lower():
                    if "leather" in nowmat.name.lower() or "scabbard" in nowmat.name.lower():
                        #move on
                        print ("Not metal because of material in sword")
                    elif "stone" in nowmat.name.lower() or "jewel" in nowmat.name.lower():
                        #move on
                        print ("Not metal because of material in sword")
                    else:
                        bi_metal = 'yes'
                        bi_image = 'no'
                        roughval = 0.033
                        edge_weapon = 'yes'
                        wr.write("  Converted to Metal due to Material Name Sword" + "\n")
                    
                    
                if "blade" in nowmat.name.lower():
                    bi_metal = 'yes'
                    bi_image = 'no'
                    bi_matt = 'no'
                    roughval = 0.033
                    metal_type = 'silver'
                    edge_weapon = 'yes'
                    wr.write("  Converted to Metal due to Material Name Blade" + "\n")
                if "hilt" in nowmat.name.lower() and edge_weapon == 'yes':
                    bi_metal = 'yes'
                    bi_image = 'no'
                    bi_matt = 'no'
                    roughval = 0.033
                    wr.write("  Converted to Metal due to Material Name hilt" + "\n")
                if "pommel" in nowmat.name.lower() and edge_weapon == 'yes':
                    bi_metal = 'yes'
                    bi_image = 'no'
                    bi_matt = 'no'
                    roughval = 0.033
                    wr.write("  Converted to Metal due to Material Name pommel" + "\n")
                
                if "metal" in nowmat.name.lower():
                    bi_metal = 'yes'
                    bi_matt = 'no'
                    roughval = 0.11
                    if metal_type == 'nothing':
                        wr.write("  Since metal in name, what is type: " + str(metal_type) + "\n")
                        if matdcolor.r == 1.0 and matdcolor.g == 1.0:
                            if matdcolor.b == 1.0:
                                dcolor_r = 0.30
                                dcolor_g = 0.30
                                dcolor_b = 0.30
                                matdcolor[0] = 0.30
                                matdcolor[1] = 0.30
                                matdcolor[2] = 0.30
                                metal_type = "aluminium"
                            else:
                                metalcol = (matdcolor)
                                bi_metalx = 'yes'
                                metal_type = 'fresnelcolor'
                                print ("Metal Type is fresnelcolor")
                                wr.write("  Converted to fresnelcolor " + "\n")
                        else:
                            if dcolor_r != dcolor_g:
                                metal_type = 'fresnelcolor'
                                metalcol = (matdcolor)
                                wr.write("  Converted to fresnelcolor " + "\n")
                    else:
                        wr.write("  Converted, both metal and this: " + str(metal_type) + "\n")
                        
                    
                    wr.write("  This is Material Name used for change: " + str(material_name) + "\n")
                    wr.write("  Converted to Metal due to Material Name Metal" + "\n")
                
                if "bottle" in nowmat.name.lower():
                    if bi_glass == 'no':
                        bi_glass = 'yes'
                        bi_matt = 'no'
                        bi_gloss = 'no'
                        alphaslot = imgslot
                        #transmitt_col = (0.9, 0.9, 0.9)
        
                #======================================================================================   
                
                wr.write("  After Overrides, Diffuse Color: " + str(matdcolor) + "\n")
                #Material component checks
                if bi_matt == 'yes':
                    roughval = 0.42
                    dcolor_a = 1.0
                    #Have to check why I did this.....
                    wr.write("  After bi_matt, Diffuse Color: " + str(matdcolor) + "\n")
                if bi_metal == 'yes':
                    if roughval == 0:
                        roughval = 0.12
                    else:
                        #roughval already calced
                        print ("Rough Val already")
                    dcolor_a = 1.0
                    bi_matt = 'no'
                    if metal_type == 'gold':
                        bi_metal = 'yes'
                        bi_matt = 'no'
                        dcolor_r = 0.5
                        dcolor_g = 0.5
                        dcolor_b = 0.3
                    if metal_type == 'silver':
                        bi_metal = 'yes'
                        bi_matt = 'no'
                        dcolor_r = 0.7
                        dcolor_g = 0.7
                        dcolor_b = 0.7
                    if metal_type == 'copper':
                        bi_metal = 'yes'
                        bi_matt = 'no'
                        dcolor_r = 0.5
                        dcolor_g = 0.4
                        dcolor_b = 0.2
                    if metal_type == 'pewter':
                        bi_metal = 'yes'
                        bi_matt = 'no'
                        dcolor_r = 0.3
                        dcolor_g = 0.3
                        dcolor_b = 0.3
                    if metal_type == 'fresnelcolor':
                        bi_metalx = 'yes'
                        bi_matt = 'no'
                        if metalcol[0] == 0.0 and metalcol[1] == 0.0:
                            if metalcol[2] == 0.0:
                                dcolor_r = matdcolor[0]
                                dcolor_g = matdcolor[1]
                                dcolor_b = matdcolor[2]
                        else:
                            dcolor_r = metalcol[0]
                            dcolor_g = metalcol[1]
                            dcolor_b = metalcol[2]
                    
                if bi_gloss == 'yes' and roughval == 0.0:
                    roughval = 0.20
                    wr.write("  After bi_gloss, Diffuse Color: " + str(matdcolor) + "\n")
                if bi_glass == 'yes' and bi_diffuse == 'no':
                    roughval = 0.001
                    dcolor_a = 0.9
                    bi_matt = 'no'   #changed 12/19/2015
                    if bi_lucent == 'yes':
                        bi_lucent = 'no'
                    if bi_mlucent == 'yes':
                        bi_mlucent = 'no'
                
                if bi_skin == 'yes':
                    if "lips" in nowmat.name.lower():
                        roughval = 0.19
                        bumpfactor = 0.65
                    else:
                        roughval = 0.220
                        bumpfactor = 0.135
                    texnormalfact = 0.14
                    mat_depth = skin_depth
                    #bumpnum = (bi_dimensionX/50) * texnormalfact
                    
                    wr.write("  Skin Bump factor: " + str(bumpfactor) + "\n")
                    diff_color = (0.335, 0.208, 0.133)
                    absorb_col = (0.034, 0.042, 0.062)
                    matspeccol = (0.050, 0.033, 0.026)
                    transmitt_col = (0.110, 0.013, 0.010)
                    bi_matt = 'no'
                    bi_lucent = 'yes'
                if bi_emitt == 'yes':
                    if power == 0:
                        power = 70
                    if matemitt > 0 and matemitt < 1.0:
                        # this has to be in watts essentially based on color
                        power = (((1 - matemitt)/2) + matemitt) * 10
                    else:
                        # This material never had emitt value before
                        power = 50
                    if matdcolor.r != 1.0 or matdcolor.g != 1.0:
                        if matdcolor.b != 1.0:
                            if matdcolor.r > 0.1 and matdcolor.g > 0.1:
                                if light_color[0] == 0.0 and light_color[1] == 0.0:
                                    light_color = ((matdcolor.r + ((1 - matdcolor.r)/2)), (matdcolor.g + ((1 - matdcolor.g)/2)), (matdcolor.b + ((1 - matdcolor.b)/2)))
                                    wr.write("  Changing emitter to existing diffuse color.... " + "\n")
                    if matdcolor.r == 1.0 and matdcolor.g == 1.0:
                        if matdcolor.b == 1.0:
                            # No original color - changing
                            if light_color[0] == 0.0 and light_color[1] == 0.0:
                                light_color = (0.3, 0.3, 0.17)
                                wr.write("  No original color, making light/lamp color... " + "\n")
                if bi_image == 'yes' and bi_diffuse == 'no':
                    # removed this as you can have image alpha instead, or image and glass
                    # bi_image = 'no'
                    wr.write("  You can have an image that is not diffuse.... " + "\n")
                if bi_alpha == 'yes' and bi_lucent == 'yes':
                    bi_lucent = 'no'
                    bi_matt = 'yes'
                    
                print ("Material Takeaways ----------------------------")
                wr.write("  Material Variables From Original Scene -----------------" + "\n")
                print ("bi_glass: " + bi_glass)
                wr.write("  bi_glass:" + bi_glass + "\n")
                print ("bi_matt: " + bi_matt)
                wr.write("  bi_matt:" + bi_matt + "\n")
                print ("bi_gloss: " + bi_gloss)
                wr.write("  bi_gloss:" + bi_gloss + "\n")
                print ("bi_reflect: " + bi_reflect)
                wr.write("  bi_reflect:" + bi_reflect + "\n")
                print ("bi_mirror: " + bi_mirror)
                wr.write("  bi_mirror:" + bi_mirror + "\n")
                print ("bi_lucent: " + bi_lucent)
                wr.write("  bi_lucent:" + bi_lucent + "\n")
                wr.write("  bi_mlucent:" + bi_mlucent + "\n")
                print ("bi_metal: " + bi_metal)
                wr.write("  bi_metal:" + bi_metal + "\n")
                print ("bi_metalx: " + bi_metalx)
                wr.write("  bi_metalx:" + bi_metalx + "\n")
                if bi_metal == 'yes':
                    wr.write("  Metal Type: " + metal_type + "\n")
                print ("   Metal type: " + metal_type)
                wr.write("  bi_emitt:" + bi_emitt + "\n")
                print ("bi_emitt: " + bi_emitt)
                print ("bi_diffuse: " + bi_diffuse)
                wr.write("  bi_diffuse:" + bi_diffuse + "\n")
                print ("bi_image: " + bi_image)
                wr.write("  bi_image:" + bi_image + "\n")
                print ("bi_bump: " + bi_bump)
                wr.write("  bi_bump:" + bi_bump + "\n")
                print ("bi_specular: " + bi_specular)
                wr.write("  bi_specular:" + bi_specular + "\n")
                print ("bi_normal: " + bi_normal)
                wr.write("  bi_normal:" + bi_normal + "\n")
                print ("bi_alpha: " + bi_alpha)
                wr.write("  bi_alpha:" + bi_alpha + "\n")
                wr.write("  Material Final - Uses Glass Image Mapping: " + use_glass_image + "\n")
                wr.write("  After Material changes, Diffuse Color: " + str(matdcolor) + "\n")  
                if bi_bump == 'no' and bi_normal == 'no':
                    print ("Warning: No Bump or Normal on this material!")
                    wr.write("  Warning: No Bump or Normal on this material!" + "\n")

                
#Shaders and Nodes here.................................................................................................     
                # Luxrender nodes
                shaderX = 625
                shaderY = 275
                L = L + 1
                lux_output = 'yes'
                sc.render.engine = 'LUXRENDER_RENDER'
                nowmat.use_nodes = True
                
                ctx_mat = nowmat.luxrender_material
                #ctx_img = nowmat.texture_slots[0]
                #ctx_bump = nowmat.texture_slots[1]
                dcolor_r = matdcolor[0]
                dcolor_g = matdcolor[1]
                dcolor_b = matdcolor[2]
                print (" ")
                print ("OUTPUT: Creating LUX Materials")
                wr.write("  OUTPUT: Creating LUX Materials....." + "\n")
                print ("FEEDBACK: starting Lux Material is " + ctx_mat.type)
                if fj >= 0:
                    print ("Image slot 0: " + str(nowmat.texture_slots[imgslot]))
# Rendering Node Section (LuxCore first)----------------------------------------------------------------------------------------------------
                # Lets figure out Luxcore and classic here.......................................................
                if luxcore_run == 'yes':    # checked off on GUI
                    #luxcore time stamp
                    corestartnum = int(time.time())
                    # LuxCore Conversion.................
                    bpy.context.scene.luxrender_engine.selected_luxrender_api = 'luxcore'
                    print("Doing Luxcore conversions")
                    wr.write("  OUTPUT: LuxCore Material Conversion chosen" + "\n")
                    wr.write("  Just changed to luxcore, Diffuse Color: " + str(matdcolor) + "\n")
                    # Luxcore Matte
                    print("Checking if matte:")
                    if bi_matt == 'yes':
                        ctx_mat.type = 'glossy'  # because matt is artificial in nature, there is shine to everything
                        editor_type = ctx_mat.luxrender_mat_glossy
                        wr.write("  Before transparent shadows...., Diffuse Color: " + str(matdcolor) + "\n")
                        nowmat.use_transparent_shadows = True
                        # There is a little shine in all things in real life
                        bi_matt = 'no' #Clearing material
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_lucent = 'no'
                        wr.write("  Mat type is glossy now, Diffuse Color: " + str(matdcolor) + "\n")
                        speccol_r = matspeccol[0] * 0.6
                        speccol_g = matspeccol[1] * 0.6
                        speccol_b = matspeccol[2] * 0.6
                        if speccol_r == 0:
                            speccol_r = 0.012
                        if speccol_g == 0:
                            speccol_g = 0.012
                        if speccol_b == 0:
                            speccol_b = 0.012    
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        print ("Spec red: " + str(speccol_r))
                        print ("Matd Color: " + str(matdcolor))
                        print ("Matd Color Index 0: " + str(matdcolor[0]))
                        print ("Matd Color red: " + str(matdcolor[0]))
                        wr.write("  Matte Color: " + str(matdcolor) + "\n")
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.2, 0.2, 0.2)
                        matspeccol = (speccol_r, speccol_g, speccol_b)
                        wr.write("  Matte Spec Color: " + str(matspeccol) + "\n")
                        use_shader = 1
                    # Luxcore Glossy
                    print("Checking if glossy:")
                    if bi_gloss == 'yes' and use_shader == 0:
                        ctx_mat.type = 'glossy'
                        editor_type = ctx_mat.luxrender_mat_glossy
                        nowmat.use_transparent_shadows = True
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_matt = 'no'
                        bi_mlucent = 'no'
                        bi_lucent = 'no'
                        speccol_r = matspeccol[0] * 0.9
                        speccol_g = matspeccol[1] * 0.9
                        speccol_b = matspeccol[2] * 0.9
                        if speccol_r == 0:
                            speccol_r = 0.12
                        if speccol_g == 0:
                            speccol_g = 0.12
                        if speccol_b == 0:
                            speccol_b = 0.12  
                        matspeccol = (speccol_r, speccol_g, speccol_b)
                        wr.write("  Gloss Spec Color: " + str(matspeccol) + "\n")
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.2, 0.2, 0.2)
                        use_shader = 1
                    # Luxcore Display (true Matte)
                    print("Checking if display:")
                    if  bi_display == 'yes':
                        ctx_mat.type = 'matte'
                        editor_type = ctx_mat.luxrender_mat_matte
                        
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_matt = 'no'
                        bi_mlucent = 'no'
                        bi_lucent = 'no'
                        bi_emitt = 'no'
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.01, 0.01, 0.01)
                        use_shader = 1
                    #luxcore Metal
                    print("Checking if metal:")
                    if bi_metal == 'yes' and use_shader == 0:
                        if bi_metalx == 'yes':
                            print ("Found to be metal2 by term metalx")
                            wr.write("  Found to be Metal2 by term metalx " + "\n")
                            ctx_mat.type = 'metal2'
                            editor_type = ctx_mat.luxrender_mat_metal2
                            bi_gloss = 'no'
                            bi_reflect = 'no'
                            use_shader = 1
                        else:
                            print ("Not found to be metalx")
                            wr.write("  Not found to be Metal2, but this is LuxCore " + "\n")
                            ctx_mat.type = 'metal2'
                            editor_type = ctx_mat.luxrender_mat_metal2
                            bi_gloss = 'no'
                            bi_metal = 'no'
                            bi_reflect = 'no'
                            use_shader = 1
                    # LuxCore Mirror
                    print("Checking if mirror:")
                    if bi_mirror == 'yes' and use_shader == 0:
                        ctx_mat.type = 'mirror'
                        editor_type = ctx_mat.luxrender_mat_mirror
                        bi_metal = 'no'
                        bi_reflect = 'no'
                        bi_gloss = 'no'
                        bi_mirror = 'no'
                        use_shader = 1
                    print("Checking if reflect:")
                    if bi_reflect == 'yes' and use_shader == 0:
                        ctx_mat.type = 'glossy'
                        editor_type = ctx_mat.luxrender_mat_glossy
                        roughval = 0.10
                        bi_gloss = 'no'
                        use_shader = 1
                        
                    print("Checking if glass and not using shader:")
                    
                    if bi_glass == 'yes' and use_shader == 0:
                        print("--Inside the If")
                        editor_type = ctx_mat.luxrender_mat_glass
                        print("--just changed editor_type")
                        try:
                            ctx_mat.type = 'glass'
                        except:
                            print ("Doesn't like changing ctx_mat.type to Glass!!")
                            wr.write("  Tried to change to glass, but failed! " + "\n")
                        print("--Changed ctx_mat type")
                        #transmission color should be high
                        
                        bi_glass = 'no'
                        bi_lucent = 'no'
                        bi_matt = 'no'
                        print("--about to make it use shader...")
                        use_shader = 1
                        print ("Because override Glass and use-shader was no. Now bi_glass = no")
                        wr.write("  Because override Glass and use-shader was no. Now bi_glass = no" + "\n")
                    
                    # print ("Code does not like that editor_type is Glass!")
                    print("Checking if glossy translucent:")
                    if bi_lucent == 'yes'and use_shader == 0:
                        editor_type = ctx_mat.luxrender_mat_glossytranslucent
                        ctx_mat.type = 'glossytranslucent'
                        #nowmat.use_transparent_shadows = True
                        #roughval = 0.225
                        bi_lucent = 'no'
                        use_shader = 1
                    print("Checking if matte translucent:")
                    if bi_mlucent == 'yes' and use_shader == 0:
                        editor_type = ctx_mat.luxrender_mat_mattetranslucent
                        ctx_mat.type = 'mattetranslucent'
                        #nowmat.use_transparent_shadows = True
                        #roughval = 0.225
                        bi_mlucent = 'no'
                        use_shader = 1
                    if matnull == 'yes':
                        editor_type = ctx_mat.luxrender_mat_null
                        ctx_mat.type = 'null'
                        bi_bump = 'no'
                        bi_alpha = 'no'
                            
                            
                    #Luxcore make nodes
                    # Here is the base Material Node Group
                    wr.write("  Node Materials Start ******************************************** " + "\n")
                    TreeNodes = bpy.data.node_groups.new(object_name + '.' + str(matSlot), type='luxrender_material_nodes')
                    try:
                        print ("Node Space is: " + TreeNodes.name)
                        wr.write("  Node Space: " + str(TreeNodes.name) + "\n")
                    except:
                        wr.write("  Unicode Error with Node Space! " + "\n")
                    TreeNodes.use_fake_user = True
                    context_data.luxrender_material.nodetree = TreeNodes.name
                    
                    print ("node type: " + str(TreeNodes.type))
                    wr.write("  Node Type: " + str(TreeNodes.type) + "\n")
                    # Shader Output Node
                    matoutX = 800
                    matoutY = 250
                    sh_out = TreeNodes.nodes.new('luxrender_material_output_node')
                    sh_out.location = matoutX,matoutY
                    lux_output = 'yes'
                    matSlot = matSlot + 1
                    #Dynamic Shader Node
                    if use_shader == 1:
                        shaderX = matoutX - 430
                        shaderY = matoutY - 15
                        use_shader = 0
                    
                    shader = TreeNodes.nodes.new('luxrender_material_' + ctx_mat.type + '_node')
                    shader.location = shaderX,shaderY
                    TreeNodes.links.new(shader.outputs[0],sh_out.inputs[0])
                    print ("Current Node location: " + str(shaderX) + ", " + str(shaderY))
                    print (" ")
                    print ("FEEDBACK: Luxrender EDITOR TYPE is  " + str(editor_type))
                    wr.write("  Base Shader is: " + str(ctx_mat.type) + "\n")
                    wr.write("  EDITOR TYPE: " + str(editor_type) + "\n")
                    # Check Materials properties
            
                    if editor_type == ctx_mat.luxrender_mat_glossy:
                        wr.write("  Running Shader: " + "glossy" + "\n")
                        shader.inputs[6].uroughness = roughval
                        shader.inputs[0].color = diff_color
                        shader.inputs[2].color = matspeccol
                        shader.inputs[4].color = absorb_col
                        shader.inputs[5].d = mat_depth
                        
                    if editor_type == ctx_mat.luxrender_mat_matte:
                        wr.write("  Running Shader: " + "matte" + "\n")
                        # This is only for a display like TV. etc.
                        shader.inputs[0].color = diff_color
                    
                    # if editor_type == ctx_mat.luxrender_mat_metal:
                    #     wr.write("  Running Shader: " + "metal" + "\n")
                    #     shader.inputs[0].uroughness = roughval
                    #     shader.metal_preset = metal_type
                        
                    if editor_type == ctx_mat.luxrender_mat_glass:
                        wr.write("  Setting up Glass..... " + "\n")
                        wr.write("  Running Shader: " + "glass" + "\n")
                        if glassarch == 'yes':
                            shader.arch = True
                        shader.inputs[0].color = matdcolor
                        shader.inputs[9].default_value = opacity
                        if use_glass_image == 'no':
                            bi_image = 'no'
                            bi_alpha = 'no'
                            wr.write("  Forced turn off image map because of GUI checkbox!! " + "\n")
                        
                    if editor_type == ctx_mat.luxrender_mat_metal2:
                        wr.write("  Running Shader: " + "metal2" + "\n")
                        
                        shader.inputs[2].uroughness = roughval
                        if bi_image == 'yes':
                            wr.write("  There is a diffuse image, but checking if there is a preset metal... " + "\n")
                            if metal_type == 'nothing':
                                wr.write("  Seems metal type is nothing -> " + str(metal_type) + "\n")
                                shader.input_type = 'color'
                                # wr.write("  Using this image on metal2: " + str(mapimage) + "\n")
                                metal_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                                shaderX -= 285
                                shaderY -= 75
                                metal_image.location = (shaderX, shaderY)
                                metal_image.image_name = nowmat.texture_slots[imgslot].texture.image.name
                                metal_image.filename = nowmat.texture_slots[imgslot].texture.image.filepath
                                newmetimg = bpy.data.images.load(filepath = teximgpath)
                                metal_image.image = newmetimg
                                TreeNodes.links.new(metal_image.outputs[0], shader.inputs[0])
                                
                            else:
                                if metal_type != 'fresnelcolor':
                                    print ("we have previously found metal preset")
                                    wr.write("  Seems metal type has been gathered -> " + str(metal_type) + "\n")
                                    shader.input_type = 'fresnel'
                                    fresnel_node = TreeNodes.nodes.new('luxrender_texture_fresnelname_node')
                                    fresnel_node.frname_preset = metal_type
                                    TreeNodes.links.new(fresnel_node.outputs[0], shader.inputs[1])
                                else:
                                    bi_metalx = 'yes'
                            bi_image = 'no'
                        else:
                            if bi_metalx == 'yes':
                                wr.write("  Seems metal type color or fresnel (bi_metalx) -> " + str(bi_metalx) + "\n")
                                shader.input_type = 'color'
                                shader.inputs[0].default_value = metalcol
                            
                            else:
                                wr.write("  Seems metal type has been gathered -> " + str(metal_type) + "\n")
                                
                                shader.input_type = 'fresnel'
                                fresnel_node = TreeNodes.nodes.new('luxrender_texture_fresnelname_node')
                                fresnel_node.frname_preset = metal_type
                                TreeNodes.links.new(fresnel_node.outputs[0], shader.inputs[1])
                            bi_image = 'no'
    
                    if editor_type == ctx_mat.luxrender_mat_glossytranslucent:
                        wr.write("  Running Shader: " + "glossytranslucent" + "\n")
                        #shader.inputs.new('luxrender_TF_uroughness_socket', 'U-Roughness')
                        shader.inputs[6].uroughness = roughval
                        
                        shader.inputs[0].color = diff_color
                        shader.inputs[1].color = transmitt_col
                        shader.inputs[2].color = matspeccol
                        shader.inputs[4].color = absorb_col
                        shader.inputs[5].d = mat_depth
                        
                    if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                        wr.write("  Running Shader: " + "mattetranslucent" + "\n")
                        #shader.inputs.new('luxrender_TF_uroughness_socket', 'U-Roughness')
                        shader.inputs[0].color = (0.05, 0.05, 0.05)
                        shader.inputs[1].color = transmitt_col
                    
                    #LuxCore Image and Bumps
                    baseX = shaderX
                    baseY = shaderY
                    # Display or Visual wall
                    if bi_display == 'yes':
                            print ("Display object material....")
                            wr.write("  This is Image for Display ... " + "\n")
                            disp_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                            shaderX -= 285
                            shaderY -= 75
                            disp_image.location = shaderX,shaderY
                            disp_image.image_name = nowmat.texture_slots[imgslot].texture.image.name
                            wr.write(" Associating Image to Display: " + str(nowmat.texture_slots[imgslot].texture.image.name) + "\n")
                            disp_image.filename = nowmat.texture_slots[imgslot].texture.image.filepath
                            disp_image.outputs.new('NodeSocketFloat', 'Color')
                            disp_image.gain = 2.10
                            TreeNodes.links.new(disp_image.outputs[0], shader.inputs[0])
                            imgalready = 'yes'
                            
                    if bi_image == 'yes' and bi_alpha == 'yes':
                        # Alpha transparency (jpg)
                        #  Must unhook shader connection
                        matmix2_node = TreeNodes.nodes.new('luxrender_material_mix_node')
                        matmixX = matoutX - 210
                        matmixY = matoutY - 2
                        matmix2_node.location = matmixX, matmixY
                        null2_node = TreeNodes.nodes.new('luxrender_material_null_node')
                        nullX = matmixX - 180
                        nullY = matmixY - 5
                        null2_node.location = nullX, nullY
                        alphaimg_node = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                        shaderX -= 315
                        shaderY += 95
                        alphgain = alphaimg_node.gain
                        alphgain = alphgain * 3
                        alphaimg_node.image_name = nowmat.texture_slots[alphaslot].texture.image.name
                        alphaimg_node.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                        wr.write("  Checking alpha filename: " + str(alphaimg_node.filename) + "\n")
                        #alphaimg_node.outputs.new('NodeSocketFloat', 'Float')
                        alphaimg_node.variant = 'float'
                        alphaimg_node.channel = 'mean'
                        alphaimg_node.gain = alphgain
                        try:
                          newimg = bpy.data.images.load(filepath = texalphapath)
                          alphaimg_node.image = newimg
                          wr.write("  First Transparent Apha at: " + str(texalphapath) + "\n")
                        except:
                          e = sys.exc_info()[0]
                          print ("Image Map Error: " + str(e))
                          wr.write("  Image Map Error: " + str(e) + "\n")
                          wr.write("  Most likely bad filepath!! " + "\n")
                        wr.write("  should have added image node now....." + "\n")
                        alphaimg_node.location = shaderX,shaderY
                        v = shader.outputs[0].links[0]
                        TreeNodes.links.remove(v)
                        TreeNodes.links.new(shader.outputs[0], matmix2_node.inputs[2])
                        TreeNodes.links.new(matmix2_node.outputs[0], sh_out.inputs[0])
                        TreeNodes.links.new(alphaimg_node.outputs[0], matmix2_node.inputs[0])
                        TreeNodes.links.new(null2_node.outputs[0], matmix2_node.inputs[1])
                        
                     
                    # Glass
                    if use_glass_image == 'yes' and ctx_mat.type == 'glass':
                        print ("Has glass image in texture slot!")
                        wr.write("  Mixing Glass Image due to texture slot and GUI checkbox... " + "\n")
                        if imgalpha == 'no':
                            glass_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                            shaderX -= 285
                            shaderY -= 75
                            glass_image.location = shaderX,shaderY
                            glass_image.image_name = nowmat.texture_slots[alphaslot].texture.image.name
                            wr.write("  Associating Image to Glass: " + str(nowmat.texture_slots[alphaslot].texture.image.name) + "\n")
                            glass_image.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_image.outputs.new('NodeSocketColor', 'Color')
                            # glass_image.outputs.variant = 'color'
                            
                            if glass_image.outputs[0]:
                                print ("Nodes slots Glass output 0: " + str(glass_image.outputs[0]))
                            TreeNodes.links.new(glass_image.outputs[0], shader.inputs[0])
                            shader.inputs[1].color = (0.7,0.7,0.7) # basic reflection
                            imgalready = 'yes'
                        if imgalpha == 'yes':
                            print ("Transparent PNG work....")
                            wr.write("  This is Transparent Image (PNG)... " + "\n")
                            # Make mix node:
                            matmix_node = TreeNodes.nodes.new('luxrender_material_mix_node')
                            matmixX = matoutX - 210
                            matmixY = matoutY - 2
                            matmix_node.location = matmixX, matmixY
                            null_node = TreeNodes.nodes.new('luxrender_material_null_node')
                            nullX = matmixX - 180
                            nullY = matmixY - 5
                            null_node.location = nullX, nullY
                            TreeNodes.links.new(matmix_node.outputs[0], sh_out.inputs[0])
                            TreeNodes.links.new(shader.outputs[0], matmix_node.inputs[2])
                            TreeNodes.links.new(null_node.outputs[0], matmix_node.inputs[1])
                            glass_mix_node = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                            imgnodeX = matmixX - 180
                            imgnodeY = matmixY + 270
                            glass_mix_node.location = imgnodeX, imgnodeY
                            glass_mix_node.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_mix_node.outputs.new('NodeSocketFloat', 'Float')
                            # glass_mix_node.outputs.remove(glass_mix_node.outputs['Color'])
                            mixgain = glass_mix_node.gain
                            mixgain = mixgain * 2.20
                            glass_mix_node.variant = 'float'
                            glass_mix_node.channel = 'alpha'
                            glass_mix_node.gain = mixgain
                            if glass_mix_node.outputs[0]:
                                print ("Nodes slots Glass output 0: " + str(glass_mix_node.outputs[0]))
                                wr.write("  Image Output Slot 0: " + str(glass_mix_node.outputs[0]) + "\n")
                                wr.write("  Variant: " + str(glass_mix_node.variant) + "\n")
                                wr.write("  Channel: " + str(glass_mix_node.channel) + "\n")
                            else:
                                print ("Something wonky with node output!!")
                            TreeNodes.links.new(glass_mix_node.outputs[0], matmix_node.inputs[0])
                            glass_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                            shaderX -= 280
                            shaderY += 5
                            glass_image.location = shaderX,shaderY
                            glass_image.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_image.outputs.new('NodeSocketFloat', 'Color')
                            # glass_image.outputs.remove(glass_image.outputs['Float'])
                            # glass_image.outputs.variant = 'color'
                            TreeNodes.links.new(glass_image.outputs[0], shader.inputs[0])
                            
                            imgalready = 'yes'
                    else:
                        #attach no image to glass material.......
                        print("   No image attached to glass.....")
            
                    # If there is an image map
                    wr.write("  Last used texture Slot: " + str(fj) + "\n")
                    if fj >= 0:
                        print ("  Slot [image]: " + str(imgslot))
                        print ("  Slot [ambient]: " + str(ambientslot))
                        print ("  Slot [bump]: " + str(bumpslot))
                        print ("  Slot [alpha]: " + str(alphaslot))
                                              
                        wr.write("  ImgAlready status: " + str(imgalready) + "\n")
                        wr.write("  bi_image status: " + str(bi_image) + "\n")
                        if bi_diffuse == 'yes':
                            wr.write("  Diffuse Map is " + str(imgslot) + ": " + str(nowmat.texture_slots[imgslot].texture.image.filepath) + "\n")
                        if texambientuse == True:
                            wr.write("  Ambient Map is "+ str(ambientslot) + ": "  + str(nowmat.texture_slots[ambientslot].texture.image.filepath) + "\n")
                        if texspecularuse == True:
                            wr.write("  Specular Map is "+ str(specslot) + ": "  + str(nowmat.texture_slots[specslot].texture.image.filepath) + "\n")
                        if bumpslot >= 0:
                            wr.write("  Bump/Normal Map is "+ str(bumpslot) + ": "  + str(nowmat.texture_slots[bumpslot].texture.image.filepath) + "\n")
                        if bi_alpha == 'yes':
                            if alphaslot >= 0:
                                wr.write("  Transparency Map is "+ str(alphaslot) + ": "  + str(nowmat.texture_slots[alphaslot].texture.image.filepath) + "\n")
                        if ctx_mat.type == 'glass' and use_glass_image == 'yes':
                            if alphaslot >= 0:
                                wr.write("  Glass Map is " + str(alphaslot) + ": " + str(nowmat.texture_slots[alphaslot].texture.image.filepath) + "\n")
                            else:
                                wr.write("  No Glass Map " + str(alphaslot) + "\n")
                             
                    # And a Bump Map  
                    if bi_bump == 'yes':
                        print ("Making a bump node, hopefully!")
                        wr.write("  Applying LuxCore Bump Map here, hopefully!! " + "\n")
                        # Add the Bump Converter
                        bump_tweak_node = TreeNodes.nodes.new('luxrender_texture_bump_map_node')
                        shaderX = baseX - 245
                        shaderY = baseY - 225
                        bump_tweak_node.location = shaderX, shaderY
                        
                        if not bump_tweak_node.outputs:
                          print("Selected node has no outputs")
                        if not bump_tweak_node.inputs:
                          print("Selected node has no inputs....adding")
                          bump_tweak_node.inputs.new('NodeSocketFloat', 'Bump Value')
                        
                        wr.write("  X Dimension: " + str(bi_dimensionX) + "\n")
                        wr.write("  Normal factor: " + str(texnormalfact) + "\n")
                        #Calculate bump nodes on anything but skin.....
                        bumpnum = (bi_dimensionX*bumpfactor) * texnormalfact
                        if bumpnum > 0.01:
                            bumpnum = 0.01
                        wr.write("  Bump Number: " + str(bumpnum) + "\n")
                        bump_tweak_node.bump_height = bumpnum
                        TreeNodes.links.new(bump_tweak_node.outputs[0], shader.inputs['Bump'])
                        # Bump Image Node
                        bump_node = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                        shaderX -= 245
                        shaderY -= 30
                        bump_node.location = shaderX, shaderY
                        # bump_node.variant = 'float'
                        bump_node.texture_coords = 'UV'
                        bump_node.gain = 0.85
                        if bi_normal == 'no':
                            bump_node.is_normal_map = False
                        if bi_normal == 'yes':
                            bump_node.is_normal_map = True
                        bump_node.variant = 'float'
                        bump_node.channel = 'mean'    
                        bump_node.image_name = nowmat.texture_slots[bumpslot].texture.image.name
                        bump_node.filename = nowmat.texture_slots[bumpslot].texture.image.filepath
                        wr.write("  What is this output anyway on bump image: " + str(bump_node.outputs[0]) + "\n")
                        TreeNodes.links.new(bump_node.outputs[0], bump_tweak_node.inputs[0])
                        bb = bump_node.outputs.keys()
                        # bump_node.inputs.new('luxrender_coordinate_socket', '2D Coordinate')
                        # bump_node.outputs.new('NodeSocketFloat', 'Float')
                        
                    if bi_image == 'yes' and imgalready == 'no':
                        if bi_ambient == 'no' and bi_diffuse == 'yes':
                            # Sometimes an ambient image will be in slot before diffuse image                  
                            color_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                            shaderX -= 315
                            shaderY += 95
                            wr.write("  should have added image node now....." + "\n")
                            color_image.location = shaderX,shaderY
                            color_image.image_name = nowmat.texture_slots[imgslot].texture.image.name
                            wr.write("  Associating Image Name to Node: " + str(nowmat.texture_slots[imgslot].texture.image.name) + "\n")
                            color_image.filename = nowmat.texture_slots[imgslot].texture.image.filepath
                            try:
                              newimg = bpy.data.images.load(filepath = teximgpath)
                              color_image.image = newimg
                            except:
                              e = sys.exc_info()[0]
                              print ("Image Map Error: " + str(e))
                              wr.write("  Image Map Error: " + str(e) + "\n")
                              wr.write("  Most likely bad filepath!! " + "\n")
                            # We go ahead and connect the node noodle anyway  
                            color_image.texture_coords = 'UV'
                            s = color_image.outputs.keys()
                            print ("Color image keys before: " + str(s))
                            if not color_image.outputs:
                               print("Selected node has no outputs") 
                            if not 'Color' in s:
                             color_image.outputs.new('NodeSocketColor', 'Color')
                            print ("Image name: " + color_image.name)
                            print ("Color image output: " + str(color_image.outputs[0]))
                            if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                                shader.inputs[1].color = transmitt_col
                                TreeNodes.links.new(color_image.outputs[0], shader.inputs[1])
                               
                            else:
                               TreeNodes.links.new(color_image.outputs[0], shader.inputs[0])
                            
              
                        bi_image = 'no'
                        imgalready = 'yes'
                    # Now this is for ambient map
                    if bi_ambient == 'yes':
                                              
                        ambi_image = TreeNodes.nodes.new('luxrender_texture_blender_image_map_node')
                        shaderX -= 315
                        shaderY += 95
                        wr.write("  should have added image node now....." + "\n")
                        ambi_image.location = shaderX,shaderY
                        ambi_image.image_name = nowmat.texture_slots[imgslot].texture.image.name
                        wr.write("  Associating Image Name to Node: " + str(nowmat.texture_slots[ambientslot].texture.image.name) + "\n")
                        ambi_image.filename = nowmat.texture_slots[ambientslot].texture.image.filepath
                        try:
                          newimg = bpy.data.images.load(filepath = texambipath)
                          ambi_image.image = newimg
                        except:
                          e = sys.exc_info()[0]
                          print ("Image Map Error: " + str(e))
                          wr.write("  Image Map Error: " + str(e) + "\n")
                        # We go ahead and connect the node noodle anyway  
                        ambi_image.texture_coords = 'UV'
                        s = ambi_image.outputs.keys()
                        print ("Color image keys before: " + str(s))
                        if not ambi_image.outputs:
                           print("Selected node has no outputs") 
                        if not 'Color' in s:
                         ambi_image.outputs.new('NodeSocketColor', 'Color')
                        print ("Image name: " + ambi_image.name)
                        print ("Color image output: " + str(ambi_image.outputs[0]))
                        if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                            shader.inputs[1].color = transmitt_col
                            TreeNodes.links.new(ambi_image.outputs[0], shader.inputs[1])
                           
                        else:
                           TreeNodes.links.new(ambi_image.outputs[0], shader.inputs[0])
                        bi_ambient = 'no'
                    
                                             
                    
                    
                    if bi_emitt == 'yes':
                        # This should setup node
                        light_node = TreeNodes.nodes.new('luxrender_light_area_node')
                        lghtnodeX = matoutX - 240
                        lghtnodeY = matoutY - 190
                        light_node.location = lghtnodeX, lghtnodeY
                        light_node.inputs[0].color = light_color
                        TreeNodes.links.new(light_node.outputs[0], sh_out.inputs[1])
                        light_node.power = power
                                             
                        bi_emitt = 'no'
                        print ("Check this ----------------------")
                        print ("Inputs/Outputs bump_node: ")
                        print (light_node.inputs[0])
                        print (light_node.outputs[0])
                    if bi_3Dtex == 'VORONOI':
                        print ("Making 3D text or procedural, hopefully!")
                        #
                        proTex_node = TreeNodes.nodes.new('luxrender_texture_blender_voronoi_node')
                        shaderX -= 315
                        shaderY += 95
                        proTex_node.location = shaderX, shaderY
                        proTex_node.noisesize = bi_texsize
                        proTex_node.variant = 'float'
                        proTex_node.minkowsky_exp = 2.5
    
                        TreeNodes.links.new(proTex_node.outputs[0], shader.inputs[5])
                        #coordinate node
                        proCoord_node = TreeNodes.nodes.new('luxrender_3d_coordinates_node')
                        shaderX -= 315
                        shaderY -= 130
                        proCoord_node.location = shaderX, shaderY
                        proCoord_node.coordinates = "uv"
                        proCoord_node.scale[0] = bi_texscaleX
                        proCoord_node.scale[1] = bi_texscaleY
                        proCoord_node.scale[2] = bi_texscaleZ
                        TreeNodes.links.new(proCoord_node.outputs[0], proTex_node.inputs[0])
                        nowthing.data.luxrender_mesh.subdiv = 'loop'
                        nowthing.data.luxrender_mesh.sublevels = 2
                        nowthing.data.luxrender_mesh.dm_floattexturename = "Texture"
                        nowthing.data.luxrender_mesh.dmscale = 0.18
                        #-----------------------------------------------------------------------
                    if bi_3Dtex == 'STUCCI':
                        print ("Making 3D text or procedural, hopefully!")
                        #
                        proTex_node = TreeNodes.nodes.new('luxrender_texture_blender_stucci_node')
                        shaderX -= 315
                        shaderY += 95
                        proTex_node.location = shaderX, shaderY
                        proTex_node.noisesize = bi_texsize
                        proTex_node.variant = 'float'
                        proTex_node.turbulence = bi_turbulence
    
                        TreeNodes.links.new(proTex_node.outputs[0], shader.inputs[5])
                        #coordinate node
                        proCoord_node = TreeNodes.nodes.new('luxrender_3d_coordinates_node')
                        shaderX -= 315
                        shaderY -= 130
                        proCoord_node.location = shaderX, shaderY
                        proCoord_node.coordinates = "uv"
                        proCoord_node.scale[0] = bi_texscaleX
                        proCoord_node.scale[1] = bi_texscaleY
                        proCoord_node.scale[2] = bi_texscaleZ
                        TreeNodes.links.new(proCoord_node.outputs[0], proTex_node.inputs[0])
                        nowthing.data.luxrender_mesh.subdiv = 'loop'
                        nowthing.data.luxrender_mesh.sublevels = 2
                        nowthing.data.luxrender_mesh.dm_floattexturename = "Texture"
                        nowthing.data.luxrender_mesh.dmscale = 0.18
                    
                    # Done with LuxCore conversions here
#-----------------CLASSIC NODES BELOW---------------------------------------------------
                else:
                    #Do the classic here
                    # Classic API Node Work---------------------------------------------------------------------------------------    
                    classicstartnum = int(time.time())
                    wr.write("  OUTPUT: Lux Classic Material Conversion chosen" + "\n")
                    bpy.context.scene.luxrender_engine.selected_luxrender_api = 'classic'
                    if bi_matt == 'yes':
                        ctx_mat.type = 'glossy'
                        editor_type = ctx_mat.luxrender_mat_glossy
                        nowmat.use_transparent_shadows = True
                        # There is a little shine in all things in real life
                        bi_matt = 'no' #Clearing material
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_lucent = 'no'
                        speccol_r = matspeccol[0] * 0.8
                        speccol_g = matspeccol[1] * 0.8
                        speccol_b = matspeccol[2] * 0.8
                        print ("Spec red: " + str(speccol_r))
                        print ("Matd Color: " + str(matdcolor))
                        print ("Matd Color Index 0: " + str(matdcolor[0]))
                        print ("Matd Color red: " + str(matdcolor[0]))
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.2, 0.2, 0.2)
                        matspeccol = (speccol_r, speccol_g, speccol_b)
                        use_shader = 1
                    if bi_gloss == 'yes' and use_shader == 0:
                        ctx_mat.type = 'glossy'
                        editor_type = ctx_mat.luxrender_mat_glossy
                        nowmat.use_transparent_shadows = True
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_matt = 'no'
                        bi_mlucent = 'no'
                        bi_lucent = 'no'
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.2, 0.2, 0.2)
                        use_shader = 1
                    if  bi_display == 'yes':
                        ctx_mat.type = 'matte'
                        editor_type = ctx_mat.luxrender_mat_matte
                        
                        bi_gloss = 'no'
                        bi_metal = 'no'
                        bi_matt = 'no'
                        bi_mlucent = 'no'
                        bi_lucent = 'no'
                        bi_emitt = 'no'
                        matdcolor = (dcolor_r, dcolor_g, dcolor_b)
                        if dcolor_r != 1.0 or dcolor_g != 1.0:
                            if dcolor_b != 1.0:
                                diff_color = matdcolor
                        else:
                            diff_color = (0.01, 0.01, 0.01)
                        use_shader = 1
                    if bi_metal == 'yes' and use_shader == 0:
                        if bi_metalx == 'yes':
                            print ("Found to be metal2 by term metalx")
                            wr.write("  Found to be Metal2 by term metalx " + "\n")
                            ctx_mat.type = 'metal2'
                            editor_type = ctx_mat.luxrender_mat_metal2
                            bi_gloss = 'no'
                            bi_reflect = 'no'
                            use_shader = 1
                        else:
                            print ("Not found to be metalx")
                            wr.write("  Not found to be Metal2, so just Metal " + "\n")
                            ctx_mat.type = 'metal'
                            editor_type = ctx_mat.luxrender_mat_metal
                            bi_gloss = 'no'
                            bi_metal = 'no'
                            bi_reflect = 'no'
                            use_shader = 1
                    if bi_mirror == 'yes' and use_shader == 0:
                        ctx_mat.type = 'mirror'
                        editor_type = ctx_mat.luxrender_mat_mirror
                        bi_metal = 'no'
                        bi_reflect = 'no'
                        bi_gloss = 'no'
                        bi_mirror = 'no'
                        use_shader = 1
                    if bi_reflect == 'yes' and use_shader == 0:
                        ctx_mat.type = 'glossy'
                        editor_type = ctx_mat.luxrender_mat_glossy
                        roughval = 0.10
                        bi_gloss = 'no'
                        use_shader = 1
                    if bi_glass == 'yes' and use_shader == 0:
                        editor_type = ctx_mat.luxrender_mat_glass
                        ctx_mat.type = 'glass'
                        bi_glass = 'no'
                        bi_lucent = 'no'
                        bi_matt = 'no'
                        use_shader = 1
                    if bi_lucent == 'yes'and use_shader == 0:
                        editor_type = ctx_mat.luxrender_mat_glossytranslucent
                        ctx_mat.type = 'glossytranslucent'
                        #nowmat.use_transparent_shadows = True
                        #roughval = 0.225
                        bi_lucent = 'no'
                        use_shader = 1
                    if bi_mlucent == 'yes' and use_shader == 0:
                        editor_type = ctx_mat.luxrender_mat_mattetranslucent
                        ctx_mat.type = 'mattetranslucent'
                        #nowmat.use_transparent_shadows = True
                        #roughval = 0.225
                        bi_mlucent = 'no'
                        use_shader = 1
                    if matnull == 'yes':
                        editor_type = ctx_mat.luxrender_mat_null
                        ctx_mat.type = 'null'
                    
                    
                    # NODE SECTION ############################################################################################
                    if lux_output == 'yes':
                        
                        # print ("Matslot is: " + str(matSlot))
                        # Here is the base Material Node Group
                        wr.write("  Node Materials Start ******************************************** " + "\n")
                        TreeNodes = bpy.data.node_groups.new(object_name + '.' + str(matSlot), type='luxrender_material_nodes')
                        try:
                            print ("Node Space is: " + TreeNodes.name)
                            wr.write("  Node Space: " + str(TreeNodes.name) + "\n")
                        except:
                            wr.write("  Unicode Error with Node Space! " + "\n")
                        TreeNodes.use_fake_user = True
                        context_data.luxrender_material.nodetree = TreeNodes.name
                        
                        print ("node type: " + str(TreeNodes.type))
                        wr.write("  Node Type: " + str(TreeNodes.type) + "\n")
                        # Shader Output Node
                        matoutX = 800
                        matoutY = 250
                        sh_out = TreeNodes.nodes.new('luxrender_material_output_node')
                        sh_out.location = matoutX,matoutY
                        lux_output = 'yes'
                        matSlot = matSlot + 1
                    
                    
                    
                    
                    # This is the original Material Shader node
                    if use_shader == 1:
                        shaderX = matoutX - 430
                        shaderY = matoutY - 15
                        use_shader = 0
                    shader = TreeNodes.nodes.new('luxrender_material_' + ctx_mat.type + '_node')
                    shader.location = shaderX,shaderY
                    TreeNodes.links.new(shader.outputs[0],sh_out.inputs[0])
                    print ("Current Node location: " + str(shaderX) + ", " + str(shaderY))
                    print (" ")
                    print ("FEEDBACK: Luxrender EDITOR TYPE is  " + str(editor_type))
                    wr.write("  Base Shader is: " + str(ctx_mat.type) + "\n")
                    # Check Materials properties
            
                    if editor_type == ctx_mat.luxrender_mat_glossy:
                        wr.write("  Running Shader: " + "glossy" + "\n")
                        shader.inputs[6].uroughness = roughval
                        shader.inputs[0].color = diff_color
                        shader.inputs[2].color = matspeccol
                        shader.inputs[4].color = absorb_col
                        shader.inputs[5].d = mat_depth
                        
                    if editor_type == ctx_mat.luxrender_mat_matte:
                        wr.write("  Running Shader: " + "matte" + "\n")
                        # This is only for a display like TV. etc.
                        shader.inputs[0].color = diff_color
                    
                    if editor_type == ctx_mat.luxrender_mat_metal:
                        wr.write("  Running Shader: " + "metal" + "\n")
                        shader.inputs[0].uroughness = roughval
                        shader.metal_preset = metal_type
                        
                    if editor_type == ctx_mat.luxrender_mat_glass:
                        wr.write("  Setting up Glass..... " + "\n")
                        wr.write("  Running Shader: " + "glass" + "\n")
                        if glassarch == 'yes':
                            shader.arch = True
                        shader.inputs[0].color = matdcolor
                        
                    if editor_type == ctx_mat.luxrender_mat_metal2:
                        wr.write("  Running Shader: " + "metal2" + "\n")
                        nowmat.luxrender_material.nodetree = ""
                        nowmat.luxrender_material.luxrender_mat_metal2.metaltype = 'fresnelcolor'
                        nowmat.luxrender_material.luxrender_mat_metal2.uroughness_floatvalue = roughval
                        if bi_image == 'yes':
                            nowmat.luxrender_material.luxrender_mat_metal2.Kr_usecolortexture = True
                            #wr.write("  Tex Slot 1 name: " + str(imgalready) + "\n")
                            nowmat.luxrender_material.luxrender_mat_metal2.Kr_colortexturename = mapimage
                        else:
                            nowmat.luxrender_material.luxrender_mat_metal2.Kr_color = metalcol
    
                    if editor_type == ctx_mat.luxrender_mat_glossytranslucent:
                        wr.write("  Running Shader: " + "glossytranslucent" + "\n")
                        #shader.inputs.new('luxrender_TF_uroughness_socket', 'U-Roughness')
                        shader.inputs[6].uroughness = roughval
                        
                        shader.inputs[0].color = diff_color
                        shader.inputs[1].color = transmitt_col
                        shader.inputs[2].color = matspeccol
                        shader.inputs[4].color = absorb_col
                        shader.inputs[5].d = mat_depth
                        
                    if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                        wr.write("  Running Shader: " + "mattetranslucent" + "\n")
                        #shader.inputs.new('luxrender_TF_uroughness_socket', 'U-Roughness')
                        shader.inputs[0].color = (0.05, 0.05, 0.05)
                        shader.inputs[1].color = transmitt_col
                        
                                    
                    baseX = shaderX
                    baseY = shaderY
                    # Display or Visual wall
                    if bi_display == 'yes':
                            print ("Display object material....")
                            wr.write("  This is Image for Display ... " + "\n")
                            disp_image = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                            shaderX -= 285
                            shaderY -= 75
                            disp_image.location = shaderX,shaderY
                            disp_image.name = nowmat.texture_slots[imgslot].texture.image.name
                            wr.write(" Associating Image to Display: " + str(nowmat.texture_slots[imgslot].texture.image.name) + "\n")
                            disp_image.filename = nowmat.texture_slots[imgslot].texture.image.filepath
                            disp_image.outputs.new('NodeSocketFloat', 'Color')
                            disp_image.gain = 2.10
                            TreeNodes.links.new(disp_image.outputs[0], shader.inputs[0])
                            
                            
                            
                            imgalready = 'yes'
                    if bi_image == 'yes' and bi_alpha == 'yes':
                        # Alpha transparency (jpg)
                        #  Must unhook shader connection
                        matmix2_node = TreeNodes.nodes.new('luxrender_material_mix_node')
                        matmixX = matoutX - 210
                        matmixY = matoutY - 2
                        matmix2_node.location = matmixX, matmixY
                        null2_node = TreeNodes.nodes.new('luxrender_material_null_node')
                        nullX = matmixX - 180
                        nullY = matmixY - 5
                        null2_node.location = nullX, nullY
                        alphaimg_node = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                        shaderX -= 320
                        shaderY += 110
                        alphgain = alphaimg_node.gain
                        alphgain = alphgain * 3
                        alphaimg_node.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                        alphaimg_node.outputs.new('NodeSocketFloat', 'Float')
                        alphaimg_node.variant = 'float'
                        alphaimg_node.channel = 'mean'
                        alphaimg_node.gain = alphgain
                        try:
                          newimg = bpy.data.images.load(filepath = texalphapath)
                          alphaimg_node.image = newimg
                          wr.write("  Tried loading 2nd alpha mask: " + str(texalphapath) + "\n")
                        except:
                          e = sys.exc_info()[0]
                          print ("Image Map Error: " + str(e))
                          wr.write("  Image Map Error: " + str(e) + "\n")
                          wr.write("  Most likely bad filepath!! " + "\n")
                        wr.write("  should have added image node now....." + "\n")
                        alphaimg_node.location = shaderX,shaderY
                        v = shader.outputs[0].links[0]
                        TreeNodes.links.remove(v)
                        TreeNodes.links.new(shader.outputs[0], matmix2_node.inputs[2])
                        TreeNodes.links.new(matmix2_node.outputs[0], sh_out.inputs[0])
                        TreeNodes.links.new(alphaimg_node.outputs[0], matmix2_node.inputs[0])
                        TreeNodes.links.new(null2_node.outputs[0], matmix2_node.inputs[1])
                        
                     
                    # Glass
                    if use_glass_image == 'yes' and ctx_mat.type == 'glass':
                        print ("Has glass image in texture slot!")
                        wr.write("  Mixing Glass Image... " + "\n")
                        if imgalpha == 'no':
                            glass_image = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                            shaderX -= 285
                            shaderY -= 75
                            glass_image.location = shaderX,shaderY
                            glass_image.name = nowmat.texture_slots[alphaslot].texture.image.name
                            wr.write("  Associating Image to Glass: " + str(nowmat.texture_slots[alphaslot].texture.image.name) + "\n")
                            glass_image.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_image.outputs.new('NodeSocketColor', 'Color')
                            # glass_image.outputs.variant = 'color'
                            
                            if glass_image.outputs[0]:
                                print ("Nodes slots Glass output 0: " + str(glass_image.outputs[0]))
                            TreeNodes.links.new(glass_image.outputs[0], shader.inputs[0])
                            shader.inputs[1].color = (0.7,0.7,0.7) # basic reflection
                            imgalready = 'yes'
                        if imgalpha == 'yes':
                            print ("Transparent PNG work....")
                            wr.write("  This is Transparent Image (PNG)... " + "\n")
                            # Make mix node:
                            matmix_node = TreeNodes.nodes.new('luxrender_material_mix_node')
                            matmixX = matoutX - 210
                            matmixY = matoutY - 2
                            matmix_node.location = matmixX, matmixY
                            null_node = TreeNodes.nodes.new('luxrender_material_null_node')
                            nullX = matmixX - 180
                            nullY = matmixY - 5
                            null_node.location = nullX, nullY
                            TreeNodes.links.new(matmix_node.outputs[0], sh_out.inputs[0])
                            TreeNodes.links.new(shader.outputs[0], matmix_node.inputs[2])
                            TreeNodes.links.new(null_node.outputs[0], matmix_node.inputs[1])
                            glass_mix_node = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                            imgnodeX = matmixX - 180
                            imgnodeY = matmixY + 270
                            glass_mix_node.location = imgnodeX, imgnodeY
                            glass_mix_node.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_mix_node.outputs.new('NodeSocketFloat', 'Float')
                            # glass_mix_node.outputs.remove(glass_mix_node.outputs['Color'])
                            mixgain = glass_mix_node.gain
                            mixgain = mixgain * 2.20
                            glass_mix_node.variant = 'float'
                            glass_mix_node.channel = 'alpha'
                            glass_mix_node.gain = mixgain
                            if glass_mix_node.outputs[0]:
                                print ("Nodes slots Glass output 0: " + str(glass_mix_node.outputs[0]))
                                wr.write("  Image Output Slot 0: " + str(glass_mix_node.outputs[0]) + "\n")
                                wr.write("  Variant: " + str(glass_mix_node.variant) + "\n")
                                wr.write("  Channel: " + str(glass_mix_node.channel) + "\n")
                            else:
                                print ("Something wonky with node output!!")
                            TreeNodes.links.new(glass_mix_node.outputs[0], matmix_node.inputs[0])
                            glass_image = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                            shaderX -= 280
                            shaderY += 5
                            glass_image.location = shaderX,shaderY
                            glass_image.filename = nowmat.texture_slots[alphaslot].texture.image.filepath
                            glass_image.outputs.new('NodeSocketFloat', 'Color')
                            # glass_image.outputs.remove(glass_image.outputs['Float'])
                            # glass_image.outputs.variant = 'color'
                            TreeNodes.links.new(glass_image.outputs[0], shader.inputs[0])
                            
                            imgalready = 'yes'
                            
            
                    # If there is an image map
                    wr.write("  Last used texture Slot: " + str(fj) + "\n")
                    if fj >= 0:
                        
                        wr.write("  ImgAlready status: " + str(imgalready) + "\n")
                        wr.write("  bi_image status: " + str(bi_image) + "\n")
                        if bi_diffuse == 'yes':
                            wr.write("  Diffuse Map is " + str(imgslot) + ": " + str(nowmat.texture_slots[imgslot].texture.image.filepath) + "\n")
                        if texambientuse == True:
                            wr.write("  Ambient Map is "+ str(ambientslot) + ": "  + str(nowmat.texture_slots[ambientslot].texture.image.filepath) + "\n")
                        if texspecularuse == True:
                            wr.write("  Specular Map is "+ str(specslot) + ": "  + str(nowmat.texture_slots[specslot].texture.image.filepath) + "\n")
                        if bi_bump == 'yes':
                            wr.write("  Bump/Normal Map is "+ str(bumpslot) + ": "  + str(nowmat.texture_slots[bumpslot].texture.image.filepath) + "\n")
                        if bi_alpha == 'yes':
                            if alphaslot >= 0:
                                wr.write("  Transparency Map is "+ str(alphaslot) + ": "  + str(nowmat.texture_slots[alphaslot].texture.image.filepath) + "\n")
                        if ctx_mat.type == 'glass':
                            if alphaslot >= 0:
                                wr.write("  Glass Map is " + str(alphaslot) + ": " + str(nowmat.texture_slots[alphaslot].texture.image.filepath) + "\n")
                             
                    # And a Bump Map  
                    if bi_bump == 'yes':
                        print ("Making a bump node, hopefully!")
                        wr.write("  Applying Classic Bump Map here, hopefully!! " + "\n")
                        # Add the Bump Converter
                        bump_tweak_node = TreeNodes.nodes.new('luxrender_texture_bump_map_node')
                        shaderX = baseX - 245
                        shaderY = baseY - 225
                        bump_tweak_node.location = shaderX, shaderY
                        
                        if not bump_tweak_node.outputs:
                          print("Selected node has no outputs")
                        if not bump_tweak_node.inputs:
                          print("Selected node has no inputs....adding")
                          bump_tweak_node.inputs.new('NodeSocketFloat', 'Bump Value')
                        
                        wr.write("  X Dimension: " + str(bi_dimensionX) + "\n")
                        wr.write("  Normal factor: " + str(texnormalfact) + "\n")
                        
                        wr.write("  Bump Number: " + str(bumpnum) + "\n")
                        bump_tweak_node.bump_height = bumpnum
                        TreeNodes.links.new(bump_tweak_node.outputs[0], shader.inputs['Bump'])
                        # Bump Image Node
                        bump_node = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                        shaderX -= 245
                        shaderY -= 30
                        bump_node.location = shaderX, shaderY
                        bump_node.variant = 'float'
                        bump_node.texture_coords = 'UV'
                        bump_node.gain = 0.85
                        bump_node.name = nowmat.texture_slots[bumpslot].texture.image.name
                        bump_node.filename = nowmat.texture_slots[bumpslot].texture.image.filepath
                        if not bump_node.outputs:
                          # Shows no output even though it is already on the GUI, now it will show two
                          print("Selected node has no outputs...adding")
                          bump_node.outputs.new('NodeSocketFloat', 'Float')
                        TreeNodes.links.new(bump_node.outputs[0], bump_tweak_node.inputs[0])
                        bb = bump_node.outputs.keys()
                        #bump_node.inputs.new('luxrender_coordinate_socket', '2D Coordinate')
                        bump_node.outputs.new('NodeSocketFloat', 'Float')
                        
                    if bi_image == 'yes' and imgalready == 'no':
                        if bi_ambient == 'no' and bi_diffuse == 'yes':
                            # Sometimes an ambient image will be in slot before diffuse image                  
                            color_image = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                            shaderX -= 315
                            shaderY += 95
                            wr.write("  should have added image node now....." + "\n")
                            color_image.location = shaderX,shaderY
                            color_image.name = nowmat.texture_slots[imgslot].texture.image.name
                            wr.write("  Associating Image Name to Node: " + str(nowmat.texture_slots[imgslot].texture.image.name) + "\n")
                            color_image.filename = nowmat.texture_slots[imgslot].texture.image.filepath
                            try:
                              newimg = bpy.data.images.load(filepath = teximgpath)
                              color_image.image = newimg
                            except:
                              e = sys.exc_info()[0]
                              print ("Image Map Error: " + str(e))
                              wr.write("  Image Map Error: " + str(e) + "\n")
                              wr.write("  Most likely bad filepath!! " + "\n")
                            # We go ahead and connect the node noodle anyway  
                            color_image.texture_coords = 'UV'
                            s = color_image.outputs.keys()
                            print ("Color image keys before: " + str(s))
                            if not color_image.outputs:
                               print("Selected node has no outputs") 
                            if not 'Color' in s:
                             color_image.outputs.new('NodeSocketColor', 'Color')
                            print ("Image name: " + color_image.name)
                            print ("Color image output: " + str(color_image.outputs[0]))
                            if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                                shader.inputs[1].color = transmitt_col
                                TreeNodes.links.new(color_image.outputs[0], shader.inputs[1])
                               
                            else:
                               TreeNodes.links.new(color_image.outputs[0], shader.inputs[0])
                            
              
                        bi_image = 'no'
                        imgalready = 'yes'
                    # Now this is for ambient map
                    if bi_ambient == 'yes':
                                              
                        ambi_image = TreeNodes.nodes.new('luxrender_texture_image_map_node')
                        shaderX -= 315
                        shaderY += 95
                        wr.write("  should have added image node now....." + "\n")
                        ambi_image.location = shaderX,shaderY
                        ambi_image.name = nowmat.texture_slots[imgslot].texture.image.name
                        wr.write("  Associating Image Name to Node: " + str(nowmat.texture_slots[ambientslot].texture.image.name) + "\n")
                        ambi_image.filename = nowmat.texture_slots[ambientslot].texture.image.filepath
                        try:
                          newimg = bpy.data.images.load(filepath = texambipath)
                          ambi_image.image = newimg
                        except:
                          e = sys.exc_info()[0]
                          print ("Image Map Error: " + str(e))
                          wr.write("  Image Map Error: " + str(e) + "\n")
                        # We go ahead and connect the node noodle anyway  
                        ambi_image.texture_coords = 'UV'
                        s = ambi_image.outputs.keys()
                        print ("Color image keys before: " + str(s))
                        if not ambi_image.outputs:
                           print("Selected node has no outputs") 
                        if not 'Color' in s:
                         ambi_image.outputs.new('NodeSocketColor', 'Color')
                        print ("Image name: " + ambi_image.name)
                        print ("Color image output: " + str(ambi_image.outputs[0]))
                        if editor_type == ctx_mat.luxrender_mat_mattetranslucent:
                            shader.inputs[1].color = transmitt_col
                            TreeNodes.links.new(ambi_image.outputs[0], shader.inputs[1])
                           
                        else:
                           TreeNodes.links.new(ambi_image.outputs[0], shader.inputs[0])
                        bi_ambient = 'no'
                    
                                             
                    
                    
                    if bi_emitt == 'yes':
                        # This should setup node
                        light_node = TreeNodes.nodes.new('luxrender_light_area_node')
                        lghtnodeX = matoutX - 240
                        lghtnodeY = matoutY - 190
                        light_node.location = lghtnodeX, lghtnodeY
                        light_node.inputs[0].color = light_color
                        TreeNodes.links.new(light_node.outputs[0], sh_out.inputs[1])
                                             
                        bi_emitt = 'no'
                        print ("Check this ----------------------")
                        print ("Inputs/Outputs bump_node: ")
                        print (light_node.inputs[0])
                        print (light_node.outputs[0])
                    if bi_3Dtex == 'VORONOI':
                        print ("Making 3D text or procedural, hopefully!")
                        #
                        proTex_node = TreeNodes.nodes.new('luxrender_texture_blender_voronoi_node')
                        shaderX -= 315
                        shaderY += 95
                        proTex_node.location = shaderX, shaderY
                        proTex_node.noisesize = bi_texsize
                        proTex_node.variant = 'float'
                        proTex_node.minkowsky_exp = 2.5
    
                        TreeNodes.links.new(proTex_node.outputs[0], shader.inputs[5])
                        #coordinate node
                        proCoord_node = TreeNodes.nodes.new('luxrender_3d_coordinates_node')
                        shaderX -= 315
                        shaderY -= 130
                        proCoord_node.location = shaderX, shaderY
                        proCoord_node.coordinates = "uv"
                        proCoord_node.scale[0] = bi_texscaleX
                        proCoord_node.scale[1] = bi_texscaleY
                        proCoord_node.scale[2] = bi_texscaleZ
                        TreeNodes.links.new(proCoord_node.outputs[0], proTex_node.inputs[0])
                        nowthing.data.luxrender_mesh.subdiv = 'loop'
                        nowthing.data.luxrender_mesh.sublevels = 2
                        nowthing.data.luxrender_mesh.dm_floattexturename = "Texture"
                        nowthing.data.luxrender_mesh.dmscale = 0.18
                        #-----------------------------------------------------------------------
                    if bi_3Dtex == 'STUCCI':
                        print ("Making 3D text or procedural, hopefully!")
                        #
                        proTex_node = TreeNodes.nodes.new('luxrender_texture_blender_stucci_node')
                        shaderX -= 315
                        shaderY += 95
                        proTex_node.location = shaderX, shaderY
                        proTex_node.noisesize = bi_texsize
                        proTex_node.variant = 'float'
                        proTex_node.turbulence = bi_turbulence
    
                        TreeNodes.links.new(proTex_node.outputs[0], shader.inputs[5])
                        #coordinate node
                        proCoord_node = TreeNodes.nodes.new('luxrender_3d_coordinates_node')
                        shaderX -= 315
                        shaderY -= 130
                        proCoord_node.location = shaderX, shaderY
                        proCoord_node.coordinates = "uv"
                        proCoord_node.scale[0] = bi_texscaleX
                        proCoord_node.scale[1] = bi_texscaleY
                        proCoord_node.scale[2] = bi_texscaleZ
                        TreeNodes.links.new(proCoord_node.outputs[0], proTex_node.inputs[0])
                        nowthing.data.luxrender_mesh.subdiv = 'loop'
                        nowthing.data.luxrender_mesh.sublevels = 2
                        nowthing.data.luxrender_mesh.dm_floattexturename = "Texture"
                        nowthing.data.luxrender_mesh.dmscale = 0.18
                        #-----------------------------------------------------------------------
                        
                # Handle on what we end up with:
                wr.write("  Current Material after algorithms: " + "\n")
                if bi_diffuse == 'yes':
                    wr.write("  Has diffuse image " + "\n")
                if bi_glass == 'yes':
                    wr.write("  Has glass " + "\n")
                    wr.write("    Uses Glass Image Mapping: " + use_glass_image + "\n")
                if bi_gloss == 'yes':
                    wr.write("  Has gloss " + "\n")
                if bi_mirror == 'yes':
                    wr.write("  Has mirror " + "\n")
                if bi_metal == 'yes':
                    wr.write("  Has metal " + "\n")
                if bi_metalx == 'yes':
                    wr.write("  Has metal2 " + "\n")
                if bi_lucent == 'yes':
                    wr.write("  Is translucent " + "\n")
                if bi_mlucent == 'yes':
                    wr.write("  Is matte translucent " + "\n")
                if bi_bump == 'yes':
                    wr.write("  Has bump map " + "\n")
                if bi_normal == 'yes':
                    wr.write("  Has non-bump normal map " + "\n")
                if bi_image == 'yes':
                    wr.write("  Has some image map " + "\n")
                if bi_ambient == 'yes':
                    wr.write("  Has ambient map " + "\n")
                if bi_specular == 'yes':
                    wr.write("  Has specular map " + "\n")
                if bi_display == 'yes':
                    wr.write("  Has a display material " + "\n")
                if bi_emitt == 'yes':
                    wr.write("  Has glow or light " + "\n")
                if matnull == 'yes':
                    wr.write("  Transparent " + "\n")    
                wr.write("  Final Diffuse Color: " + str(matdcolor) + "\n")
                wr.write("  Final Specular Color: " + str(matspeccol) + "\n")
                if ctx_mat.type == 'glossy' or ctx_mat.type == 'glossytranslucent':
                    wr.write("  Final Absorption Color: " + str(absorb_col) + "\n")
                if ctx_mat.type == 'glossy' or ctx_mat.type == 'glossytranslucent':
                    wr.write("  Final Absorption Depth: " + str(mat_depth) + "\n")
                wr.write("  Final Bump Height factor: " + str(bumpnum) + "\n")
                wr.write("  Final Roughness factor: " + str(roughval) + "\n")
                inIDX = 0
                for shadeIn in shader.inputs:
                    wr.write("  Shader Input " + str(inIDX) + ": " + str(shadeIn) + "\n")
                    inIDX = inIDX + 1
                 
                wr.write("  Node Materials Finished ******************************************** " + "\n")

                print ('FEEDBACK: End converting this material ' + str(nowmat) )
                wr.write("  Finished converting this Material: " + str(nowmat) + "\n")
                wr.write("  " + "\n")
                print ("==============================================================================================")
                j = -1
                fj = -1
                material_idx = material_idx + 1
                matnull = 'no'
                bi_bump = 'no'
                bi_glass = 'no'
                bi_skin = 'no'
                bi_metal = 'no'
                bi_metalx = 'no'
                bi_image = 'no'
                bi_diffuse = 'no'
                bi_alpha = 'no'
                bi_ambient = 'no'
                bi_normal = 'no'
                bi_display = 'no'
                teximage = 'nothing'
                bi_turbulence = 0.5
                bi_3Dtex = 'nothing'
                metal_type = 'nothing'
                cstep = 0
                bumpslot = -1
                texslot = -1
                slotNum = 0
                imgslot = -1
                normslot = -1
                specslot = -1
                ambientslot = -1
                alphaslot = -1
                glassarch = 'no'
                imgalready = 'no'
                imgalpha = 'no'
                use_glass_image = 'no'
                light_color = (0.9,0.9,0.8)
                diff_color = (0.4,0.4,0.4)
                absorb_col = (0.02, 0.02, 0.02)
                transmitt_col = (0.6, 0.6, 0.6)
                specular_col = (0.03, 0.03, 0.03)
                # matdcolor = (0.1, 0.1, 0.1)
                matdcolor = dict(zip(['r', 'g', 'b'], [0.1, 0.1, 0.1]))
                # matdcolor.r = matdcolor[0]
                # matdcolor.g = matdcolor[1]
                # matdcolor.b = matdcolor[2]
                matspeccol = (0.03, 0.03, 0.03)
                metalcol = (0.0, 0.0, 0.0)
                speccol_r = 0.03
                speccol_g = 0.03
                speccol_b = 0.03
                dcolor_r = 0.15
                dcolor_g = 0.15
                dcolor_b = 0.15
                dcolor_a = 1.0
                use_shader = 0
                spec_changed = 'no'
                edge_weapon = 'no'
                matspecinty = 0.0
                mat_depth = 0.0
                bumpnum = 0.01
                bumpfactor = 0.45
                roughval = 0
                texambientuse = False
                texspecularuse = False
                spec_changed = 'no'
                
                nowmat.use_transparency = False
                editor_type = ctx_mat.luxrender_mat_matte # default
                nowmat.diffuse_color = (0.1, 0.1, 0.1)  # reset default diffuse color
            material_idx = 0
            mirror_added = 0
            bi_emitt = 'no'
            
        
            wr.write("=========================================================" + "\n")
        
        matSlot = 0
        bi_dimensionX = 0
        bi_dimensionY = 0
        # Here we flat shade faces if LuxCore chosen
        if luxcore_run == 'yes':
            if flatshade_all == 'yes':
                if nowthing.type == 'MESH':
                    sc.objects.active = nowthing
                    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                    bpy.ops.mesh.reveal()
                    bpy.ops.mesh.select_all(action='SELECT')
                    bpy.ops.mesh.faces_shade_flat()
                    print (str(nowthing.name) + " is shaded flat now")
                    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)



            
            


        
    # We should be Done here.....
    wr.write("Finished converting the Scene! " + "\n")
    messageTXT = 'Finished!!'
    nowsec = time.strftime("%H%M%S")
    print ("Done at: " + str(nowsec))
    wr.write("Stopped at " + str(nowsec) + "\n")
    finishnum = int(time.time())
    elapsenum = (finishnum - startnum)/60
    if luxcore_run == 'yes':
        coretime = (finishnum - corestartnum)/60
        wr.write("The LuxCore API section took " + str(coretime) + " minutes \n")
    else:
        classictime = (finishnum - classicstartnum)/60
        wr.write("The Classic API section took " + str(classictime) + " minutes \n")
    wr.write("This took " + str(elapsenum) + " minutes \n")
    
    
# ----------------------------------------------------------------------------
# Windows and such.........




class cvrtLux(bpy.types.Operator):
    bl_idname = "lux.convert"
    bl_label = "Convert All Materials"
    bl_description = "Convert all materials in the scene from non-nodes to Lux"
    bl_register = True
    bl_undo = True

    @classmethod
    def poll(cls, context):
        return True
    
    def execute(self, context):
        AutoNode(False)
        return {'FINISHED'}
    
class addVolumes(bpy.types.Operator):
    bl_idname = "lux.dovolumes"
    bl_label = "Add Volume/Haze to Lux Scene"
    bl_description = "Add volumetrics and rudimentary fog/haze"
    bl_register = True
    bl_undo = True

    @classmethod
    def poll(cls, context):
        return True
    
    def execute(self, context):
        makeVolumetrics()
        return {'FINISHED'}



class mlrestore(bpy.types.Operator):
    bl_idname = "mll.restore"
    bl_label = "Restore"
    bl_description = "Switch Back to non nodes & Blender Internal"
    bl_register = True
    bl_undo = True
    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        AutoNodeOff()
        return {'FINISHED'}

from bpy.props import *
sc = bpy.types.Scene
sc.EXTRACT_ALPHA = BoolProperty(attr="EXTRACT_ALPHA", default=False)
sc.EXTRACT_PTEX = BoolProperty(attr="EXTRACT_PTEX", default=False)
sc.EXTRACT_OW = BoolProperty(attr="Overwrite", default=False, description="Extract textures again instead of re-using previous textures")
#my_bool = BoolProperty( name="Enable or Disable", description="A simple bool property",default = True)
sc.my_prop = BoolProperty( name="Prop name", description="Skip Image Mapping onto Glass", default = False)
sc.do_core = BoolProperty( name="Cvt Luxcore", description="Convert for LuxCore API", default = False)
sc.do_flat = BoolProperty( name="All Flatshade", description="Flat Shade all faces for LuxCore", default = False)
sc.messageTXT = StringProperty(name = 'FEEDBACK: ')


class OBJECT_PT_sceneall(bpy.types.Panel):
    bl_label = "Convert Materials to LuxRender"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    
    def draw(self, context):
        sc = context.scene
        layout = self.layout
        
        row = layout.row()
        # Allow user to skip original model author use of image mapping on glass
        row.prop(sc, "my_prop", text="Disable Maps on Glass")
        row = layout.row()
        box = row.box()
        box.prop(sc, "do_core", text="Materials for LuxCore")
        box.prop(sc, "do_flat", text="Flat Shade All Objects")
        row = layout.row()
        row.prop(sc, "messageTXT")
        row = layout.row()
        row.operator("lux.convert", text='Convert All Materials to Lux', icon='MATERIAL')
        
        
        
        
        
        




        
        
        



def register():
    bpy.utils.register_module(__name__)
    pass

def unregister():
    bpy.utils.unregister_module(__name__)
    pass

if __name__ == "__main__":
    register()
